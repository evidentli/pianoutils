# -*- coding: utf-8 -*-
import os
import json
import xmltodict
import xml.etree.ElementTree as Et
from unittest import TestCase
from copy import deepcopy
from piano_utils.ris_converter import ris_to_piano
from piano_utils.endnote_converter import endnote_xml_to_json, json_to_endnote_xml, \
    endnote_xml_to_piano, endnote_json_to_piano, endnote_to_ris, piano_to_endnote


class TestEndNoteConverter(TestCase):

    @classmethod
    def setUpClass(cls):
        """
        create/setup test data
        :return:
        """
        cls.ids = ["1920960", "WOS:A1992HH28100022", "MEDLINE:2095826"]
        cls.titles = [
            "Myocardial metastasis from primary lung cancer: myocardial infarction-like ECG changes and pathologic findings",
            "PNEUMOCYSTIS-CARINII PNEUMONIA - AN UNCOMMON CAUSE OF DEATH IN AFRICAN PATIENTS WITH ACQUIRED-IMMUNODEFICIENCY-SYNDROME",
            "Neoplasms metastatic to the heart: review of 3314 consecutive autopsies"
        ]
        cls.pages = ["213-8", "617-620", "195-8"]
        cls.authors = [
            ["Abe, S.", "Watanabe, N.", "Ogura, S.", "Kunikane, H.", "Isobe, H.",
             "Yamaguchi, E.", "Munakata, M.", "Kawakami, Y."],
            ["Abouya, Y. L.", "Beaumel, A.", "Lucas, S.", "Dagoakribi, A.", "Coulibaly, G.",
             "Ndhatz, M.", "Konan, J. B.", "Yapi, A.", "Decock, K. M."],
            ["Abraham, K. P.", "Reddy, V.", "Gattuso, P."]
        ]
        cls.keywords = [
            ["Adenocarcinoma/diagnosis/epidemiology/secondary", "Adult", "Aged", "Aged, 80 and over", "Diagnosis, Differential",
             "*Electrocardiography", "Female", "Heart Neoplasms/diagnosis/epidemiology/*secondary", "Humans", "Incidence",
             "*Lung Neoplasms", "Male", "Middle Aged", "Myocardial Infarction/*diagnosis", "Pericardium",
             "Prevalence", "Retrospective Studies"],
            [],
            []
        ]
        cls.dates = [
            {"year": "1991", "date": "May - Jun"},
            {"year": "1992", "date": "Mar"},
            {"year": "1990", "date": "1990"},
        ]
        cls.rec_nos = ["203", "991", "1023"]

        cls.endnote_xmls = [
            open(os.path.join(os.path.dirname(__file__), 'resources/single_endnote_xml_article.xml')).read(),
            open(os.path.join(os.path.dirname(__file__), 'resources/multiple_endnote_xml_articles.xml')).read(),
        ]

        cls.endnote_xml_non_ascii = open(os.path.join(os.path.dirname(__file__),
                                                      'resources/single_endnote_xml_article_non_ascii.xml')).read()
        cls.endnote_xml_multiple_non_ascii = open(os.path.join(os.path.dirname(__file__),
                                                      'resources/multiple_endnote_xml_articles_non_ascii.xml')).read()

        def add_style_attributes(d, keys):
            for k in keys:
                for attr, val in [("face", "normal"), ("font", "default"), ("size", "100%")]:
                    d["%s_style_@%s" % (k, attr)] = val

        cls.endnote_jsons = []
        for article_idx, _id in enumerate(cls.ids):
            endnote_json = {
                "source-app_#text": "EndNote",
                "source-app_@name": "EndNote",
                "source-app_@version": "17.3",
                "accession-num_style_#text": _id,
                "titles_title_style_#text": cls.titles[article_idx],
                "pages_style_#text": cls.pages[article_idx],
                "rec-number": cls.rec_nos[article_idx]
            }
            add_style_attributes(endnote_json, ["accession-num", "titles_title", "pages"])

            for idx, a in enumerate(cls.authors[article_idx]):
                endnote_json["contributors_authors_author_%s_style_#text" % idx] = a
                add_style_attributes(endnote_json, ["contributors_authors_author_%s" % idx])

            for idx, keyword in enumerate(cls.keywords[article_idx]):
                endnote_json["keywords_keyword_%s_style_#text" % idx] = keyword
                add_style_attributes(endnote_json, ["keywords_keyword_%s" % idx])

            date = cls.dates[article_idx]
            endnote_json["dates_year_style_#text"] = date["year"]
            endnote_json["dates_pub-dates_date_style_#text"] = date["date"]
            add_style_attributes(endnote_json, ["dates_year", "dates_pub-dates_date"])

            cls.endnote_jsons.append(endnote_json)

    def test_xml_to_json(self):
        """
        test conversion from xml to json
        :return:
        """
        original_xml = deepcopy(self.endnote_xmls[0])

        # convert xml to json
        result = json.loads(endnote_xml_to_json(original_xml))
        self.assertTrue(isinstance(result, list))

        result = result[0]

        # check is flattened
        for v in result.values():
            self.assertNotIsInstance(v, dict)
            self.assertNotIsInstance(v, list)

        # check values
        for k, v in self.endnote_jsons[0].iteritems():
            self.assertEquals(v, result[k])

    def test_xml_to_json__with_multiple(self):
        """
        test conversion from xml to json with multiple articles in xml
        :return:
        """
        original_xml = deepcopy(self.endnote_xmls[1])

        # convert xml to json
        result = json.loads(endnote_xml_to_json(original_xml))
        self.assertTrue(isinstance(result, list))

        # check is flattened
        for article in result:
            for v in article.values():
                self.assertNotIsInstance(v, dict)
                self.assertNotIsInstance(v, list)

        # check values
        for idx, endnote_json in enumerate(self.endnote_jsons):
            for k, v in endnote_json.iteritems():
                self.assertEquals(v, result[idx][k])

    def check_xml(self, article_idx, xml_string):
        original_json = self.endnote_jsons[article_idx]

        result_tree = Et.fromstring(xml_string)
        records = result_tree.find("records")
        record = records[article_idx]

        # check source-app
        self.assertEquals("EndNote", record.find("source-app").text)
        self.assertEquals(original_json["source-app_@version"], record.find("source-app").attrib["version"])

        # check accession-num
        self.assertEquals(self.ids[article_idx], record.find(".//accession-num/style").text)

        # check title
        self.assertEquals(self.titles[article_idx], record.find(".//titles/title/style").text)

        # check authors
        authors = record.find(".//contributors/authors")
        for idx, author in enumerate(authors.findall(".//author/style")):
            self.assertEquals(original_json["contributors_authors_author_%s_style_#text" % idx],
                              author.text)

        # check pagination
        self.assertEquals(self.pages[article_idx], record.find(".//pages/style").text)

        # check keywords
        if self.keywords[article_idx]:
            keywords = record.find(".//keywords")
            self.assertIsNotNone(keywords)
            for idx, keyword in enumerate(keywords.findall(".//keyword/style")):
                self.assertEquals(original_json["keywords_keyword_%s_style_#text" % idx],
                                  keyword.text)

        # check dates
        self.assertEquals(self.dates[article_idx]["year"], record.find(".//dates/year/style").text)
        self.assertEquals(self.dates[article_idx]["date"], record.find(".//dates/pub-dates/date/style").text)

    def test_json_to_xml(self):
        """
        test conversion from json to xml
        :return:
        """
        original_json = deepcopy(self.endnote_jsons[0])

        # convert from json to xml
        result = json_to_endnote_xml(json.dumps(original_json))

        self.check_xml(0, result)

    def test_json_to_xml__with_multiple(self):
        """
        test conversion from json to xml with multiple articles
        :return:
        """
        original_json = deepcopy(self.endnote_jsons)

        # convert from json to xml
        result = json_to_endnote_xml(json.dumps(original_json))

        for idx, _ in enumerate(original_json):
            self.check_xml(idx, result)

    def test_xml_to_json_to_xml(self):
        """
        test conversion from xml to json, then back from json to xml to ensure they are equal
        :return:
        """
        original_xml = deepcopy(self.endnote_xmls[0])

        # convert xml to json
        xml_as_json = json.loads(endnote_xml_to_json(deepcopy(original_xml)))

        # convert json back to xml
        json_as_xml = json_to_endnote_xml(json.dumps(xml_as_json[0]))

        # format result and expected result into dictionaries
        result = json.loads(json.dumps(xmltodict.parse(json_as_xml)))
        expects = json.loads(json.dumps(xmltodict.parse(original_xml)))

        # compare dictionary results
        self.assertDictEqual(result, expects)

    def test_xml_to_json_to_xml__with_multiple(self):
        """
        test conversion from xml to json, then back from json to xml with multiple articles
        :return:
        """
        original_xml = deepcopy(self.endnote_xmls[1])

        # convert xml to json
        xml_as_json = json.loads(endnote_xml_to_json(deepcopy(original_xml)))

        # convert json back to xml
        json_as_xml = json_to_endnote_xml(json.dumps(xml_as_json))

        # format result and expected result into dictionaries
        result = json.loads(json.dumps(xmltodict.parse(json_as_xml)))
        expects = json.loads(json.dumps(xmltodict.parse(original_xml)))

        # compare dictionary values
        self.assertDictEqual(result, expects)

    def test_json_to_xml_to_json(self):
        """
        test conversion from json to xml, then back from xml to json to ensure they are equal
        :return:
        """
        original_json = deepcopy(self.endnote_jsons[0])

        # convert json to xml
        json_as_xml = json_to_endnote_xml(json.dumps(original_json))

        # convert xml back to json
        xml_as_json = json.loads(endnote_xml_to_json(json_as_xml))

        # compare results
        self.assertDictEqual(original_json, xml_as_json[0])

    def test_json_to_xml_to_json__with_multiple(self):
        """
        test conversion from json to xml, then back from xml to json with multiple articles
        :return:
        """
        original_json = deepcopy(self.endnote_jsons)

        # convert json to xml
        json_as_xml = json_to_endnote_xml(json.dumps(original_json))

        # convert xml back to json
        xml_as_json = json.loads(endnote_xml_to_json(json_as_xml))

        # compare results
        self.assertEquals(len(original_json), len(xml_as_json))
        for idx, d in enumerate(original_json):
            self.assertDictEqual(d, xml_as_json[idx])

    def check_piano(self, article_idx, doc):
        """
        perform tests on a piano document against the test data
        :param article_idx: index of article in test data
        :param doc: piano document to check
        :return:
        """
        # check external id
        self.assertEquals(self.ids[article_idx], doc.get("external_id", ""))

        # check title
        self.assertEquals(self.titles[article_idx], doc.get("title", ""))

        # check authors
        self.assertEquals(json.dumps(self.authors[article_idx]), doc.get("authors", ""))

        # check pages
        self.assertEquals(self.pages[article_idx], doc.get("pages", ""))

        # check keywords
        if self.keywords[article_idx]:
            self.assertEquals(",".join(self.keywords[article_idx]), doc.get("keywords", ""))

        # check rec-no (field that isn't explicitly transformed/mapped
        self.assertEquals(self.rec_nos[article_idx], doc.get("rec-number"))

    def test_xml_to_piano(self):
        """
        test conversion from xml to piano
        :return:
        """
        original_xml = deepcopy(self.endnote_xmls[0])

        # convert xml to piano documents
        piano_docs = endnote_xml_to_piano(original_xml)
        self.assertEquals(1, len(piano_docs))
        self.check_piano(0, piano_docs[0])

    def test_xml_to_piano__with_multiple(self):
        """
        test conversion from xml to piano with multiple articles
        :return:
        """
        original_xml = deepcopy(self.endnote_xmls[1])

        # convert xml to piano documents
        piano_docs = endnote_xml_to_piano(original_xml)
        self.assertEquals(len(self.endnote_jsons), len(piano_docs))
        for idx, _ in enumerate(self.endnote_jsons):
            self.check_piano(idx, piano_docs[idx])

    def test_json_to_piano(self):
        """
        test conversion from json to piano
        :return:
        """
        original_json = deepcopy(self.endnote_jsons[0])

        # convert json to piano documents
        piano_docs = endnote_json_to_piano(json.dumps(original_json))
        self.assertEquals(1, len(piano_docs))
        self.check_piano(0, piano_docs[0])

    def test_json_to_piano__with_multiple(self):
        """
        test conversion from json to piano with multiple articles
        :return:
        """
        original_json = deepcopy(self.endnote_jsons)

        # convert json to piano documents
        piano_docs = endnote_json_to_piano(json.dumps(original_json))
        self.assertEquals(len(self.endnote_jsons), len(piano_docs))
        for idx, _ in enumerate(self.endnote_jsons):
            self.check_piano(idx, piano_docs[idx])

    def test_xml_to_json_to_piano__with_non_ascii_char(self):
        """
        test conversion from xml to json, then json to piano where xml contents contains non-ascii character(s)
        :return:
        """
        original_xml = deepcopy(self.endnote_xml_non_ascii)

        # convert xml to json
        json_string = endnote_xml_to_json(original_xml)
        # convert json to piano
        piano = endnote_json_to_piano(json_string)
        article = piano[0]

        # check values
        self.assertEquals(self.ids[0], article["external_id"])
        self.assertEquals(self.titles[0], article["title"])

    def test_xml_to_json_to_piano__with_multiple_non_ascii_char(self):
        """
        test conversion from xml to json, then json to piano where xml contents contains non-ascii character(s)
        :return:
        """
        original_xml = deepcopy(self.endnote_xml_multiple_non_ascii)

        # convert xml to json
        json_string = endnote_xml_to_json(original_xml)
        # convert json to piano
        piano = endnote_json_to_piano(json_string)

        # check values
        self.assertEquals(len(self.endnote_jsons), len(piano))
        for idx, _ in enumerate(self.endnote_jsons):
            self.assertEquals(self.ids[idx], piano[idx]["external_id"])
            self.assertEquals(self.titles[idx], piano[idx]["title"])


class TestEndNoteRISConverter(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.single_endnote_xml = open(os.path.join(os.path.dirname(__file__), 'resources/single_endnote_xml_article.xml')).read()
        cls.single_endnote_xml_non_ascii = open(os.path.join(os.path.dirname(__file__), 'resources/single_endnote_xml_article_non_ascii.xml')).read()
        cls.multiple_endnote_xml_non_ascii = open(os.path.join(os.path.dirname(__file__), 'resources/multiple_endnote_xml_articles_non_ascii.xml')).read()

        cls.single_endnote_ris = """
TY  - Journal Article
AB  - Myocardial metastasis from neoplastic disease is often clinically unapparent, and very difficult to diagnose. Of 151 consecutive autopsies of lung cancer patients, cardiac metastases were found in 67 patients (44.4%). Myocardial metastasis was found in only 8 patients (11.9%). ECG of patients with myocardial metastasis revealed ST-T wave changes and various types of arrhythmia. ST-T wave changes were observed in 4 with myocardial metastasis, and in 6 without myocardial metastasis (pericardial metastasis alone). ST-T wave changes is not a specific finding of myocardial metastasis. Two very rare cases with myocardial metastasis showing progressive ST segment elevation with a QS pattern are presented. The appearance of ST segment elevation with a QS pattern in clinically stable lung cancer patients without cardiac symptoms suggestive of myocardial injury indicates the possibility of myocardial metastasis. Myocardial metastasis is often elusive, thus careful observation of ECG changes is of primary importance for the antemortem diagnosis.
AD  - First Department of Medicine, School of Medicine, Hokkaido University, Sapporo, Japan.
AN  - 1920960
AU  - Abe, S.
AU  - Watanabe, N.
AU  - Ogura, S.
AU  - Kunikane, H.
AU  - Isobe, H.
AU  - Yamaguchi, E.
AU  - Munakata, M.
AU  - Kawakami, Y.
DB  - NLM
DO  - 10.1016/j.pto.2017.03.002
ET  - 1991/05/01
IS  - 3
J1  - Japanese journal of medicine
J2  - Japanese journal of medicine
KW  - Adenocarcinoma/diagnosis/epidemiology/secondary
KW  - Adult
KW  - Aged
KW  - Aged, 80 and over
KW  - Diagnosis, Differential
KW  - *Electrocardiography
KW  - Female
KW  - Heart Neoplasms/diagnosis/epidemiology/*secondary
KW  - Humans
KW  - Incidence
KW  - *Lung Neoplasms
KW  - Male
KW  - Middle Aged
KW  - Myocardial Infarction/*diagnosis
KW  - Pericardium
KW  - Prevalence
KW  - Retrospective Studies
LA  - eng
LB  - Pubmed 20160527
N1  - Abe, S Watanabe, N Ogura, S Kunikane, H Isobe, H Yamaguchi, E Munakata, M Kawakami, Y Case Reports Journal Article Japan Jpn J Med. 1991 May-Jun;30(3):213-8.
PY  - 1991 May - Jun
SN  - 0021-5120 (Print)
                    0021-5120
SP  - 213-8
TI  - Myocardial metastasis from primary lung cancer: myocardial infarction-like ECG changes and pathologic findings
T2  - Jpn J Med
VL  - 30
ER  - """

        cls.single_endnote_ris_non_ascii = """
TY  - Journal Article
AB  - Myocardial metastasis from neoplastic disease is often clinically unapparent, and very difficult to diagnose. Of 151 consecutive autopsies of lung cancer patients, cardiac metastases were found in 67 patients (44.4%). Myocardial metastasis was found in only 8 patients (11.9%). ECG of patients with myocardial metastasis revealed ST-T wave changes and various types of arrhythmia ∼ 10-15%. ST-T wave changes were observed in 4 with myocardial metastasis, and in 6 without myocardial metastasis (pericardial metastasis alone). ST-T wave changes is not a specific finding of myocardial metastasis. Two very rare cases with myocardial metastasis showing progressive ST segment elevation with a QS pattern are presented. The appearance of ST segment elevation with a QS pattern in clinically stable lung cancer patients without cardiac symptoms suggestive of myocardial injury indicates the possibility of myocardial metastasis. Myocardial metastasis is often elusive, thus careful observation of ECG changes is of primary importance for the antemortem diagnosis.
AD  - First Department of Medicine, School of Medicine, Hokkaido University, Sapporo, Japan.
AN  - 1920960
AU  - Abe, S.
AU  - Watanabe, N.
AU  - Ogura, S.
AU  - Kunikane, H.
AU  - Isobe, H.
AU  - Yamaguchi, E.
AU  - Munakata, M.
AU  - Kawakami, Y.
DB  - NLM
ET  - 1991/05/01
IS  - 3
J1  - Japanese journal of medicine
J2  - Japanese journal of medicine
KW  - Adenocarcinoma/diagnosis/epidemiology/secondary
KW  - Adult
KW  - Aged
KW  - Aged, 80 and over
KW  - Diagnosis, Differential
KW  - *Electrocardiography
KW  - Female
KW  - Heart Neoplasms/diagnosis/epidemiology/*secondary
KW  - Humans
KW  - Incidence
KW  - *Lung Neoplasms
KW  - Male
KW  - Middle Aged
KW  - Myocardial Infarction/*diagnosis
KW  - Pericardium
KW  - Prevalence
KW  - Retrospective Studies
LA  - eng
LB  - Pubmed 20160527
N1  - Abe, S Watanabe, N Ogura, S Kunikane, H Isobe, H Yamaguchi, E Munakata, M Kawakami, Y Case Reports Journal Article Japan Jpn J Med. 1991 May-Jun;30(3):213-8.
PY  - 1991 May-Jun
SN  - 0021-5120 (Print) 0021-5120
SP  - 213-8
TI  - Myocardial metastasis from primary lung cancer: myocardial infarction-like ECG changes and pathologic findings
T2  - Jpn J Med
VL  - 30
ER  - """.decode("utf-8")

        cls.multiple_endnote_ris = ["""
TY  - Journal Article
AB  - Admissions and deaths in a pulmonary medicine ward in Abidjan, Cote d'lvoire, West Africa, were evaluated over a 6-month period in 1989 with systematic autopsies on all patients who died. Of 473 patients admitted, 38% were HIV-1 seropositive, 4% were HIV-2 seropositive, and 14% reacted to both viruses. A total of 100 patients (21%) died, and deaths were twice as frequent in HIV-seropositive compared with HIV-negative patients. The pathology of 78 autopsies showed that the predominant cause of death in HIV-seropositive patients was disseminated tuberculosis (40%). Cancer was the cause of death in 64% of HIV-negative patients. Pneumocystosis was found in only 9% of HIV-seropositive autopsies. Since Pneumocystis carinii is an uncommon cause of death in this population, prophylaxis for P. carinii pneumonia is not warranted for HIV-infected patients in Africa. In contrast, research on chemoprophylaxis for tuberculosis is urgently required.
AN  - WOS:A1992HH28100022
AU  - Abouya, Y. L.
AU  - Beaumel, A.
AU  - Lucas, S.
AU  - Dagoakribi, A.
AU  - Coulibaly, G.
AU  - Ndhatz, M.
AU  - Konan, J. B.
AU  - Yapi, A.
AU  - Decock, K. M.
IS  - 3
LB  - Web of Science 20160527
N1  - Times Cited: 122 0 122
PY  - 1992 Mar
SN  - 0003-0805
SP  - 617-620
TI  - PNEUMOCYSTIS-CARINII PNEUMONIA - AN UNCOMMON CAUSE OF DEATH IN AFRICAN PATIENTS WITH ACQUIRED-IMMUNODEFICIENCY-SYNDROME
T2  - American Review of Respiratory Disease
UR  - <Go to ISI>://WOS:A1992HH28100022
VL  - 145
ER  - """,
"""
TY  - Journal Article
AB  - Cardiac involvement by metastatic neoplasms is relatively uncommon and usually occurs with widely disseminated disease ∼ 10-15%. Ninety-five cases with cardiac metastases from autopsies performed over a 14-year period (1974-1987) at Loyola University Medical Center are reviewed. During this period, 3314 autopsies were performed with an average annual autopsy rate of 35%. In 806 (24.3%), a malignant disease was found, and in 95 (11.8%), there was cardiac involvement by tumor. The most common malignancies encountered in order of decreasing frequency were lung, lymphoma, breast, leukemia, stomach, melanoma, liver, and colon. Although the percentage of cardiac metastasis compares favorably with previous reports in the literature, an identical rate was present during both halves of the 14-year period studied. Improved diagnostic capabilities and treatment protocols in recent years have apparently not significantly affected the incidence, distribution, or patterns of metastatic spread to the heart.
AN  - MEDLINE:2095826
AU  - Abraham, K. P.
AU  - Reddy, V.
AU  - Gattuso, P.
IS  - 3
LB  - Web of Science 20160527
N1  - Times Cited: 142 2 145
PY  - 1990 1990
SN  - 0887-8005
SP  - 195-8
TI  - Neoplasms metastatic to the heart: review of 3314 consecutive autopsies
T2  - The American journal of cardiovascular pathology
UR  - <Go to ISI>://MEDLINE:2095826
VL  - 3
ER  - """.decode("utf-8")]
        cls.multiple_endnote_ris = "\n".join([cls.single_endnote_ris_non_ascii] + cls.multiple_endnote_ris)

    def test_endnote_to_ris(self):
        """
        test conversion from endnote xml to ris string
        :return: 
        """
        result = endnote_to_ris(self.single_endnote_xml)
        self.assertEquals(self.single_endnote_ris.strip(), result.strip())

    def test_endnote_to_ris__with_non_ascii(self):
        """
        test conversion from endnote xml to ris string with non ascii character/s
        :return: 
        """
        result = endnote_to_ris(self.single_endnote_xml_non_ascii)
        self.assertEquals(self.single_endnote_ris_non_ascii.strip(), result.strip())

    def test_endnote_to_ris__with_multiple(self):
        """
        test conversion from endnote xml to ris string with multiple articles
        :return: 
        """
        result = endnote_to_ris(self.multiple_endnote_xml_non_ascii)
        self.assertEquals(self.multiple_endnote_ris.strip(), result.strip())

    def check_endnote(self, expect, actual):
        """
        perform tests on a piano document against the test data
        :param expect: expected value (piano format)
        :param actual: actual xml value
        :return:
        """
        result_tree = Et.fromstring(actual)
        records = result_tree.find("records")
        for idx, record in enumerate(records):
            ref = expect[idx]
            # check id
            if "_id" in ref:
                self.assertEquals(ref.pop("_id"), record.find(".//piano-id").text)

            # check source app
            self.assertEquals("EndNote", record.find(".//source-app").text)

            # check type
            self.assertEquals(ref.pop("TY"), record.find(".//ref-type").attrib["name"])

            # check authors
            if "AU" in ref:
                authors = [a.text for a in record.findall(".//contributors/authors/author/style")]
                self.assertEquals(ref.pop("AU"), json.dumps(authors))

            # check keywords
            if "KW" in ref:
                keywords = [a.text for a in record.findall(".//keywords/keyword/style")]
                self.assertEquals(ref.pop("KW"), json.dumps(keywords))

            # check dates
            if "PY" in ref:
                year = ref.pop("PY").split(" ")
                date = None
                if len(year) > 1:
                    date = " ".join(year[1:])
                    year = year[0]
                else:
                    year = year[0]

                if year.isdigit() and len(year) == 4:
                    self.assertEquals(year, record.find(".//dates/year/style").text)

                if date:
                    self.assertEquals(date, record.find(".//dates/pub-dates/date/style").text)

            # check titles
            if "TI" in ref:
                self.assertEquals(ref.pop("TI"), record.find(".//titles/title/style").text)
            if "T2" in ref:
                self.assertEquals(ref.pop("T2"), record.find(".//titles/secondary-title/style").text)
            if "J2" in ref:
                self.assertEquals(ref.pop("J2"), record.find(".//titles/alt-title/style").text)
            if "J1" in ref:
                self.assertEquals(ref.pop("J1"), record.find(".//periodical/abbr-1/style").text)

            # check notes
            if "N1" in ref:
                self.assertEquals(ref.pop("N1"), json.dumps([record.find(".//notes/style").text]))

            # check urls
            if "UR" in ref:
                self.assertEquals(ref.pop("UR"), record.find(".//urls/related-urls/url/style").text)

            # check other RIS fields
            for ris_key, endnote_key in [
                ("AB", "abstract"),
                ("AD", "auth-address"),
                ("AN", "accession-num"),
                ("DB", "remote-database-provider"),
                ("DO", "electronic-resource-num"),
                ("ET", "edition"),
                ("IS", "number"),
                ("LA", "language"),
                ("LB", "label"),
                ("SN", "isbn"),
                ("SP", "pages"),
                ("VL", "volume"),
                ("AB", "abstract"),
            ]:
                if ris_key in ref:
                    self.assertEquals(ref.pop(ris_key), record.find(".//%s/style" % endnote_key).text)

            # check unknown fields
            for k in ref.keys():
                ref_value = ref[k]
                if not isinstance(ref_value, basestring):
                    ref_value = json.dumps(ref_value)
                self.assertEquals(ref_value, record.find(".//%s" % k).text)

    def test_piano_to_endnote(self):
        """
        test conversion from piano to endnote xml
        :return: 
        """
        piano = ris_to_piano(self.single_endnote_ris)
        result = piano_to_endnote(piano)
        self.check_endnote(piano, result)

    def test_piano_to_endnote__with_non_ascii(self):
        """
        test conversion from piano to endnote xml with non ascii character/s
        :return: 
        """
        piano = ris_to_piano(self.single_endnote_ris_non_ascii)
        result = piano_to_endnote(piano)
        self.check_endnote(piano, result)

    def test_piano_to_endnote__with_unknown_tags(self):
        """
        test conversion from piano with unknown tags to endnote xml
        :return:
        """
        piano = ris_to_piano(self.single_endnote_ris)
        piano[0]["_id"] = "5add5c2795284700190724a0"
        piano[0]["labels"] = ["one", "two", "three"]
        piano[0]["abstrackr_status"] = {"project_1": ["Abstrackr_Rejected", "User_Accepted"]}
        piano[0]["other-custom-field"] = "other custom value"
        result = piano_to_endnote(piano)
        self.check_endnote(piano, result)

    def test_piano_to_endnote__with_multiple(self):
        """
        test conversion from piano to endnote xml with multiple articles
        :return: 
        """
        piano = ris_to_piano(self.multiple_endnote_ris)
        result = piano_to_endnote(piano)
        self.check_endnote(piano, result)

    def test_piano_to_endnote__with_multiple_with_unknown_tags(self):
        """
        test conversion from piano to endnote xml with multiple articles with unknown tags
        :return:
        """
        piano = ris_to_piano(self.multiple_endnote_ris)
        for idx, p in enumerate(piano):
            p["_id"] = str(idx)
            p["labels"] = ["label_%s" % str(idx * 2), "label_%s" % str(idx * 6)]
            p["abstrackr_status"] = {"project_1": ["Abstrackr_Rejected", "User_Accepted"]}
            p["other-custom-field"] = "other custom value"
        result = piano_to_endnote(piano)
        self.check_endnote(piano, result)

    def test_piano_to_endnote__with_bom_character(self):
        """
        test conversion from endnote to piano with BOM (Byte Order Mark) character
        :return: 
        """
        endnote = open(os.path.join(os.path.dirname(__file__),
                                    'resources/single_endnote_xml_article_bom_char.xml')).read()
        ris = ris_to_piano(endnote_to_ris(endnote))
        self.assertEquals(len(ris), 1)

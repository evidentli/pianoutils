from unittest import TestCase
from copy import deepcopy
from random import randint
import os

from piano_utils.csv_converter import csv_to_piano


TEST_RESOURCE_DIR = os.path.join(os.path.dirname(__file__), "resources/csv")

import_empty_file = open(os.path.join(TEST_RESOURCE_DIR, "csv_import_empty.csv")).read()

import_simple_file = open(os.path.join(TEST_RESOURCE_DIR, "csv_import_simple.csv")).read()
import_simple_data = [
    {"person_id": "1", "attr1": "A", "attr2": "A", "attr3": "1"},
    {"person_id": "2", "attr1": "B", "attr2": "A", "attr3": "2"},
    {"person_id": "3", "attr1": "C", "attr2": "B", "attr3": "3A"},
    {"person_id": "4", "attr1": "D", "attr2": "B", "attr3": "44"},
    {"person_id": "5", "attr1": "E", "attr2": "A", "attr3": "${test}"},
]

import_dupl_file = open(os.path.join(TEST_RESOURCE_DIR, "csv_import_dupl.csv")).read()
import_dupl_data = [
    {"person_id": "1", "attr1": "G", "attr2": "B", "attr3": "Shirley the business woman"},
    {"person_id": "2", "attr1": "K", "attr2": "B", "attr3": "Britta the Britta"},
    {"person_id": "3", "attr1": "C", "attr2": "B", "attr3": "3A"},
    {"person_id": "4", "attr1": "D", "attr2": "B", "attr3": "44"},
    {"person_id": "5", "attr1": "E", "attr2": "A", "attr3": "${test}"},
    {"person_id": "6", "attr1": "F", "attr2": "A", "attr3": "Jeff the lawyer"},
    {"person_id": "9", "attr1": "H", "attr2": "B", "attr3": "Troy the repairman"},
    {"person_id": "11", "attr1": "J", "attr2": "A", "attr3": "Annie the detective"},
    {"person_id": "13", "attr1": "L", "attr2": "B", "attr3": "Dean the dean"},
]

import_empty_cells_file = open(os.path.join(TEST_RESOURCE_DIR, "csv_import_empty_cells.csv")).read()
import_empty_cells_data = [
    {"person_id": "1", "attr1": "A", "attr2": "A", "attr3": "1"},
    {"person_id": "2", "attr1": "", "attr2": "A", "attr3": "2"},
    {"person_id": "3", "attr1": "C", "attr2": "", "attr3": "3A"},
    {"person_id": "4", "attr1": "D", "attr2": "B", "attr3": ""},
    {"person_id": "5", "attr1": "E", "attr2": "", "attr3": "${test}"},
    {"person_id": "6", "attr1": "", "attr2": "A", "attr3": "Jeff the lawyer"},
    {"person_id": "7", "attr1": "G", "attr2": "", "attr3": ""},
    {"person_id": "8", "attr1": "", "attr2": "", "attr3": ""},
    {"person_id": "9", "attr1": "I", "attr2": "A", "attr3": ""},
    {"person_id": "10", "attr1": "J", "attr2": "A", "attr3": "Annie the detective"},
]

import_empty_header_file = open(os.path.join(TEST_RESOURCE_DIR, "csv_import_simple_empty_header.csv")).read()

import_extra_cols_file = open(os.path.join(TEST_RESOURCE_DIR, "csv_import_simple_extra_cols.csv")).read()

import_special_chars_file = open(os.path.join(TEST_RESOURCE_DIR, "csv_import_special_chars.csv")).read()
import_special_chars_data = [
    {"person_id": "1", "name": "A", "desc": "Here's a non ascii character [\xd1].", "age": "22"},
    {"person_id": "2", "name": "B", "desc": '"I\'m an open quote', "age": "-"},
    {"person_id": "3", "name": "C", "desc": 'I\'m an open, quote"', "age": "65"},
    {"person_id": "4", "name": "D", "desc": "I' keep on truckin''", "age": "43"},
    {"person_id": "5", "name": "E", "desc": "I said, \"'ello!\"'", "age": "82"},
    {"person_id": "6", "name": ",", "desc": "", "age": "10,123"},
]


class TestImportCsv(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.import_special_chars_data = deepcopy(import_special_chars_data)
        cls.import_empty_cells_data = deepcopy(import_empty_cells_data)
        cls.import_dupl_data = deepcopy(import_dupl_data)
        cls.import_simple_data = deepcopy(import_simple_data)

        for file_data in [cls.import_special_chars_data, cls.import_empty_cells_data, cls.import_dupl_data, cls.import_simple_data]:
            for ref in file_data:
                if "person_id" in ref:
                    ref.pop("person_id")

    def test_csv(self, **kwargs):
        if not kwargs:
            return
        import_file = deepcopy(kwargs.get("import_file"))
        expect_data = kwargs.get("expect_data")
        _id = kwargs.get("_id")

        resources = csv_to_piano(import_file, _id)
        if _id is None:
            _id = "_id"

        self.assertIsInstance(resources, list)
        self.assertEquals(len(resources), len(expect_data))

        for idx, expect in enumerate(expect_data):
            actual = resources[idx]
            self.assertIsInstance(actual, dict)
            self.assertTrue(_id in actual)
            expect_keys = expect.keys()
            if _id not in expect:
                expect_keys.append(_id)
            self.assertEquals(sorted(actual.keys()), sorted(expect_keys))
            for k in expect.keys():
                self.assertEquals(actual[k], expect[k])

    def test_import_csv_empty(self):
        """
        test importing of csv file that is empty
        :return:
        """
        self.test_csv(import_file=import_empty_file,
                      expect_data=[])

    def test_import_csv_simple(self):
        """
        test importing of simple csv file into Piano
        :return:
        """
        self.test_csv(import_file=import_simple_file,
                      expect_data=self.import_simple_data)

    def test_import_csv_duplicate_ids(self):
        """
        test importing of csv file with duplicate keys (on multiple rows)
        :return:
        """
        self.test_csv(import_file=import_dupl_file,
                      expect_data=self.import_dupl_data)

    def test_import_csv_empty_values(self):
        """
        test importing of csv file with empty cells on certain rows
        :return:
        """
        self.test_csv(import_file=import_empty_cells_file,
                      expect_data=self.import_empty_cells_data)

    def test_import_csv_encoded_chars(self):
        """
        test importing of csv file with special/encoded/funny characters
        :return:
        """
        self.test_csv(import_file=import_special_chars_file,
                      expect_data=self.import_special_chars_data)

    def test_import_large_csv(self):
        """
        test importing of large csv file
        :return:
        """
        fields = ["cohort", "age", "alias"]
        cohorts = ["C1", "C2", "C3", "CC"]
        cohorts_len = len(cohorts) - 1
        first_names = ["Jeff", "Annie", "Troy", "Britta", "Abed", "Shirley", "John", "Jill", "Star", "Luke", "Anne", "Mark", "Neil"]
        first_names_len = len(first_names) - 1
        last_names = ["Winger", "Edison", "Barnes", "Perry", "Nadir", "Bennett", "Smith", "Pelton", "Hawthorne", "Citizen", "Burns"]
        last_names_len = len(last_names) - 1
        references = []
        for i in range(10123):
            references.append({"_id": "r" + str(i + 1),
                             "cohort": cohorts[randint(0, cohorts_len)],
                             "age": str(randint(18, 95)),
                             "alias": first_names[randint(0, first_names_len)] + " " + last_names[
                                 randint(0, last_names_len)]
                             })

        import_file = "," + ",".join(fields) + "\n"
        for p in references:
            import_file += ",".join([p["_id"], p["cohort"], p["age"], p["alias"]]) + "\n"

        self.test_csv(import_file=import_file,
                      expect_data=references)

    def test_import_csv_invalid_header(self):
        """
        test importing of csv with invalid header
        :return:
        """
        invalid_header_file = """
        ,,attr2,attr3
        1,A,A,1
        2,B,A,2"""
        self.test_csv(import_file=invalid_header_file,
                      expect_data=[])

    def test_import_csv_header_mismatch(self):
        """
        test importing of csv with header mismatching with number of columns
        :return:
        """
        self.test_csv(import_file=import_extra_cols_file,
                      expect_data=self.import_simple_data)

    def test_import_csv_empty_header_rows(self):
        """
        test importing of csv with empty rows above header
        :return:
        """
        self.test_csv(import_file=import_empty_header_file,
                      expect_data=self.import_simple_data)


class TestImportCsvPatients(TestImportCsv):

    def test_import_csv_empty(self):
        """
        test importing of csv file that is empty
        :return:
        """
        self.test_csv(import_file=import_empty_file,
                      expect_data=[], _id="person_id")

    def test_import_csv_simple(self):
        """
        test importing of simple csv file into Piano
        :return:
        """
        self.test_csv(import_file=import_simple_file,
                      expect_data=import_simple_data, _id="person_id")

    def test_import_csv_duplicate_ids(self):
        """
        test importing of csv file with duplicate keys (on multiple rows)
        :return:
        """
        self.test_csv(import_file=import_dupl_file,
                      expect_data=import_dupl_data, _id="person_id")

    def test_import_csv_empty_values(self):
        """
        test importing of csv file with empty cells on certain rows
        :return:
        """
        self.test_csv(import_file=import_empty_cells_file,
                      expect_data=import_empty_cells_data, _id="person_id")

    def test_import_csv_encoded_chars(self):
        """
        test importing of csv file with special/encoded/funny characters
        :return:
        """
        self.test_csv(import_file=import_special_chars_file,
                      expect_data=import_special_chars_data, _id="person_id")

    def test_import_large_csv(self):
        """
        test importing of large csv file
        :return:
        """
        fields = ["cohort", "age", "alias"]
        cohorts = ["C1", "C2", "C3", "CC"]
        cohorts_len = len(cohorts) - 1
        first_names = ["Jeff", "Annie", "Troy", "Britta", "Abed", "Shirley", "John", "Jill", "Star", "Luke", "Anne", "Mark", "Neil"]
        first_names_len = len(first_names) - 1
        last_names = ["Winger", "Edison", "Barnes", "Perry", "Nadir", "Bennett", "Smith", "Pelton", "Hawthorne", "Citizen", "Burns"]
        last_names_len = len(last_names) - 1
        patients = []
        for i in range(10123):
            patients.append({"person_id": "p" + str(i + 1),
                             "cohort": cohorts[randint(0, cohorts_len)],
                             "age": str(randint(18, 95)),
                             "alias": first_names[randint(0, first_names_len)] + " " + last_names[
                                 randint(0, last_names_len)]
                             })

        import_file = "," + ",".join(fields) + "\n"
        for p in patients:
            import_file += ",".join([p["person_id"], p["cohort"], p["age"], p["alias"]]) + "\n"

        self.test_csv(import_file=import_file,
                      expect_data=patients, _id="person_id")

    def test_import_csv_invalid_header(self):
        """
        test importing of csv with invalid header
        :return:
        """
        invalid_header_file = """
        ,,attr2,attr3
        1,A,A,1
        2,B,A,2"""
        self.test_csv(import_file=invalid_header_file,
                      expect_data=[], _id="person_id")

    def test_import_csv_header_mismatch(self):
        """
        test importing of csv with header mismatching with number of columns
        :return:
        """
        self.test_csv(import_file=import_extra_cols_file,
                      expect_data=import_simple_data, _id="person_id")

    def test_import_csv_empty_header_rows(self):
        """
        test importing of csv with empty rows above header
        :return:
        """
        self.test_csv(import_file=import_empty_header_file,
                      expect_data=import_simple_data, _id="person_id")

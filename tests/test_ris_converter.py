# -*- coding: utf-8 -*-
import os
import json
from unittest import TestCase
from copy import deepcopy
from piano_utils.ris_converter import ris_to_piano, piano_to_ris, RIS_KEY_MAPPING, LIST_TYPE_TAGS


class TestRISConverter(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.ris_single = open(os.path.join(os.path.dirname(__file__), 'resources/single_ris_article.ris')).read()
        cls.ris_single_id = open(os.path.join(os.path.dirname(__file__), 'resources/single_ris_article_id.ris')).read()
        cls.ris_single_non_ascii = open(os.path.join(os.path.dirname(__file__), 'resources/single_ris_article_non_ascii.ris')).read()
        cls.ris_single_unknown_tag = open(os.path.join(os.path.dirname(__file__), 'resources/single_ris_article_unknown_tag.ris')).read()
        cls.ris_multiple = open(os.path.join(os.path.dirname(__file__), 'resources/multiple_ris_articles.ris')).read()

        cls.piano_single = {
            "TY": "THES",
            "AU": '["Beh, Thian Thian"]',
            "Y2": "2017/01/04",
            "Y1": "2016",
            "UR": "http://hdl.handle.net/11343/123144",
            "AB": "The centromere plays a crucial role in genome inheritance - ensuring proper segregation of sister chromatids into each daughter cell. In cancer, especially in later stages of solid tumours, cells are often presented with extensive chromosomal abnormalities. However, the involvement of defective centromeres in the disease formation and progression has not been carefully studied. Hence, my PhD project aimed to investigate the role of defective centromeres in cancer progression using the NCI-60 panel of cancer cell lines. The spectrum of centromere-related abnormalities were first characterised with pan-centromeric FISH probes and then with the addition of CENP-A immunofluorescence. For HOP-92 and SN12C, cell lines showing high prevalence of functional dicentric chromosomes, expansion of single cell clones from the initial heterogeneous population was carried out to further study the involvement of dicentric chromosomes in cancer genomic instability. Neocentromere formation sites in cancer cell lines were investigated using cell lines with high prevalence of neocentric chromosomes. A neocentromere was found in T-47D which marked the first report of a neocentromere discovered in a long-established and widely used cell line, and also the first neocentromere to be reported in breast cancer. Separately, an antibody screen to identify antibodies recognising components of an active centromere in methanol-acetic acid stored cells was performed because thus far, antibodies that are widely used for functional centromere detection mainly worked on freshly harvested cells whereas most cytogenetic samples are stored long-term in methanol-acetic acid fixative. I found a commercially available rabbit monoclonal anti-CENP-C that worked on fixed samples and in addition, I combined three methods (FISH, immunofluorescence and mFISH) to obtain more information from the same metaphase spread. I then proceeded to test anti-CENP-C in my method, CENP-IF-cenFISH-mFISH, on dicentric- and neocentric-containing cancer cell lines and proposed other utilities for this method which I have developed.",
            "KW": '["centromere", "CENP-A", "CENP-C", "immunofluorescence", "fluorescence in situ hybridization (FISH)", "multicolour FISH (mFISH)", "neocentromere", "dicentric", "NCI-60", "cancer", "cytogenetics"]',
            "T1": "The role of centromere defects in cancer formation and progression",
            "L1": "/bitstream/handle/11343/123144/2016%20Dec%20Thian%20Beh%20-%20Thesis.pdf?sequence=1&isAllowed=n",
        }
        cls.piano_single_id = deepcopy(cls.piano_single)
        cls.piano_single_id["_id"] = "5a8c262b1dae1e0034513fa5"
        cls.piano_single_non_ascii = {
            "TY": "JOUR",
            "AU": '["Shannon,Claude E."]',
            "PY": "1948/07//",
            "TI": "A Mathematical Theory of Communication",
            "JF": "Bell System Technical Journal",
            "AB": "This is a test abstract with non-ascii character; here — it is! And another [∴] one.".decode("utf-8"),
            "SP": "379",
            "EP": "423",
            "VL": "27",
        }
        cls.piano_single_unknown_tag = {
            "TY": "Clinical Trial",
            "AU": '["Shannon,Claude E."]',
            "PY": "1948/07//",
            "TI": "A Mathematical Theory of Communication",
            "JF": "Bell System Technical Journal",
            "SP": "379",
            "EA": '["evidently 1", "evidently 2"]',
            "EP": "423",
            "VL": "27",
            "JP": "CRISPR",
            "DC": "Direct Current",
            "MV": "unknown tag",
            "abstrackr_status": "user_include",
            "labels": '["trial", "cancer"]'
        }
        cls.piano_multiple = [
            cls.piano_single,
            {
                "TY": "RPRT",
                "T1": "Rearrangement hotspots in the coding and non-coding genome drive breast carcinogenesis",
                "T2": "A somatic-mutational process recurrently duplicates germline susceptibility loci and tissue-specific super-enhancers in breast cancers",
                "A1": '["Glodzik, Dominik", "Morganella, Sandro", "Davies, Helen", "Simpson, Peter", "Li, Yilong", "Zou, Xueqing", "DiezPerez, Javier", "Staaf, Johan", "Alexandrov, Ludmil B.", "Smid, Marcel", "Brinkman, Arie", "Rye, Inga", "Russnes, Hege", "Raine, Keiran", "Purdie, Colin", "Lakhani, Sunil", "Thompson, Alastair", "Birney, Ewan", "Stunnenberg, Hendrik", "Vijver, Marc", "Martens, John", "BorresenDale, AnneLise", "Richardson, Andrea", "Kong, Gu", "Viari, Alain", "Easton, Douglas", "Evan, Gerard", "Campbell, Peter", "Stratton, Michael", "NikZainal, Serena"]',
                "AD": "Wellcome Trust Sanger Institute, Hinxton, Cambridge CB10 1SA, UK",
                "N1": '["Published as: A somatic-mutational process recurrently duplicates germline susceptibility loci and tissue-specific super-enhancers in breast cancers. Nature Genetics ; Vol.49, iss.3, p.341-348 ; January 23, 2017", "Dataset: RASSTI"]',
                "Y1": "2016-03-21",
                "SP": "30 p.",
                "M3": "Basic Biological Sciences(59) ; Biological Science",
                "SN": "LA-UR-16-21897",
                "DO": "10.1038/ng.3771",
                "UR": "http://permalink.lanl.gov/object/view?what=info:lanl-repo/lareport/LA-UR-16-21897",
            },
            {
                "TY": "JOUR",
                "ID": "12345",
                "T1": "Title of reference",
                "A1": '["Marx, Karl", "Lindgren, Astrid"]',
                "A2": '["Glattauer, Daniel"]',
                "Y1": "2014//",
                "N2": "BACKGROUND: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  RESULTS: Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. CONCLUSIONS: Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.",
                "KW": '["Pippi", "Nordwind", "Piraten"]',
                "JF": "Lorem",
                "JA": "lorem",
                "VL": "9",
                "IS": "3",
                "SP": "e0815",
                "CY": "United States",
                "PB": "Fun Factory",
                "SN": "1932-6208",
                "M1": "1008150341",
                "L2": "http://example.com",
                "UR": "http://example_url.com",
            },
            {
                "TY": "BOOK",
                "ID": "122725",
                "T1": "La survenue du cancer : effets de court et moyen termes sur l’emploi, le chômage et les arrêts maladie.".decode("utf-8"),
                "A1": '["BARNAY T.", "BEN HALIMA M.A.", "DUGUET E.", "LANFRANCHI J.", "LE CLAINCHE C."]',
                "PY": "2015/04",
                "ED": "Institut de Recherche et Documentation en Economie de la Santé. (I.R.D.E.S.). Paris. FRA".decode("utf-8"),
                "KW": '["CHOMAGE", "FRANCE", "EMPLOI", "ETAT SANTE", "AGE", "INDEMNITE JOURNALIERE", "MALADIE LONGUE DUREE", "INEGALITE DEVANT SOINS", "ARRET TRAVAIL", "IMPACT", "SEXE", "CANCER", "CONGE MALADIE", "MOYEN TERME", "GRAVITE", "LONG TERME", "ABSENTEISME"]',
                "AB": "La réduction des inégalités face à la maladie est un des attendus majeurs du troisième Plan cancer 2014-2019 qui préconise de « diminuer l’impact du cancer sur la vie personnelle » afin d’éviter la « double peine » (maladie et exclusion du marché du travail). Dans ce contexte, nous évaluons l’impact de un à cinq ans d’un primo-enregistrement en Affection de longue durée (ALD) caractérisant le cancer sur la situation professionnelle et la durée passée en emploi, maladie et chômage de salariés du secteur privé. Nous utilisons la base de données administratives Hygie, recensant la carrière professionnelle et les épisodes de maladie d’un échantillon de salariés affiliés au Régime général de la Sécurité sociale. L’évaluation de l’impact de la survenue du cancer s’appuie sur une méthode de double différence avec appariement exact pour comparer les salariés malades aux salariés sans aucune ALD. La première année après le diagnostic correspond au temps des traitements caractérisé par une augmentation du nombre de trimestres d’arrêts de travail pour maladie de 1,7 pour les femmes et de 1,2 pour les hommes. L’âge joue également un rôle sur les absences liées à la maladie. Par ailleurs, l’employabilité des travailleurs atteints du cancer diminue avec le temps. La proportion de femmes et d’hommes employés au moins un trimestre, baisse respectivement de 8 et 7 points de pourcentage dans l’année suivant la survenue du cancer et jusqu’à treize points de pourcentage cinq ans plus tard. Cette distance à l’emploi se renforce lorsque les salariés malades sont plus âgés. L’effet de la maladie à cinq ans est respectivement de 15 et 19 points de pourcentage pour les hommes de plus de 51 ans et pour les femmes de plus de 48 ans. Ces différences de genre et d’âge peuvent traduire des différences de localisation et de sévérité des cancers, d’une part, de séquelles des cancers et de difficultés de réinsertion sur le marché du travail plus importantes avec l’avancée en âge, d’autre part.".decode("utf-8"),
                "UR": "http://www.irdes.fr/recherche/documents-de-travail/065-la-survenue-du-cancer-effets-de-court-et-moyen-termes-sur-emploi-chomage-arrets-maladie.pdf",
                "N1": '["DT65"]',
                "DP": "IRDES",
                "LA": "FRE",
                "PB": "Paris : Irdes",
                "EP": "44p.",
                "T3": "Document de travail Irdes ; 65",
            }
        ]

    def check_ris_to_piano(self, expect, actual):
        self.assertTrue(actual)
        self.assertTrue(isinstance(actual, list))
        self.assertEquals(len(expect), len(actual))

        # check result can be jsonified
        json.loads(json.dumps(actual))

        for idx, ref in enumerate(expect):
            self.assertEquals(sorted(ref.keys()), sorted(actual[idx].keys()))
            for k, v in ref.iteritems():
                self.assertEquals(v, actual[idx][k])

    def check_piano_to_ris(self, expect, actual):
        expect = deepcopy(expect).decode("utf-8")
        self.assertTrue(actual)
        self.assertTrue(isinstance(actual, basestring))

        expect_lines = []
        ref_lines = []
        tag_history = []
        tag = None
        for l in expect.splitlines():
            if len(l) > 4 and l[4] == "-":
                tag = l[:2]
                if tag == "ER":
                    expect_lines.append(ref_lines)
                    ref_lines = []
                    tag_history = []
                    tag = None
                    continue
                if tag in tag_history and tag not in LIST_TYPE_TAGS:
                    continue
                tag_history.append(tag)
                ref_lines.append((tag, l[5:].lstrip()))
            elif tag is not None:
                ref_lines[-1] = (tag, ref_lines[-1][1] + l.strip())

        actual_lines = [(i[:2], i[5:].lstrip()) for i in actual.splitlines()]

        for idx, ref_lines in enumerate(expect_lines):
            for tag, value in ref_lines:
                if not tag.strip():
                    continue
                t = (tag, value)
                self.assertTrue(t in actual_lines, "Ref #%s, Value for '%s' not in result" % (idx, str(t)))

    def test_ris_to_piano(self):
        """
        test conversion from ris to piano
        :return: 
        """
        original_ris = deepcopy(self.ris_single)
        result = ris_to_piano(original_ris)
        self.check_ris_to_piano([self.piano_single], result)

    def test_ris_to_piano__with_id(self):
        """
        test conversion from ris to piano with piano id field
        :return:
        """
        original_ris = deepcopy(self.ris_single_id)
        result = ris_to_piano(original_ris)
        self.check_ris_to_piano([self.piano_single_id], result)

    def test_ris_to_piano__with_all_fields(self):
        """
        test conversion from ris to piano with article that has all available ris values
        :return:
        """
        # create ris string with all ris tags
        tags = deepcopy(RIS_KEY_MAPPING)
        [tags.pop(k) for k in ["TY", "ER", "UK"]]
        ris_string = "TY  - JOUR\n"
        for t, d in tags.iteritems():
            ris_string += "%s  - %s\n" % (t, d)
        ris_string += "ER  - \n"

        # convert ris to piano
        result = ris_to_piano(ris_string)
        self.assertTrue(result)
        self.assertTrue(isinstance(result, list))
        self.assertEquals(len(result), 1)

        # check result can be jsonified
        json.loads(json.dumps(result))

        # check values
        tags["TY"] = "JOUR"
        for t in LIST_TYPE_TAGS:
            tags[t] = json.dumps([tags[t]])
        tags["_id"] = tags.pop("PI")
        self.assertDictEqual(tags, result[0])

    def test_ris_to_piano__with_non_ascii(self):
        """
        test conversion from ris to piano with non ascii character(s) in article/reference
        :return: 
        """
        original_ris = deepcopy(self.ris_single_non_ascii)
        result = ris_to_piano(original_ris)
        self.check_ris_to_piano([self.piano_single_non_ascii], result)

    def test_ris_to_piano__with_non_ris_tags(self):
        """
        test conversion from ris to piano with unknown ris tags in article/reference
        :return:
        """
        original_ris = deepcopy(self.ris_single_unknown_tag)
        result = ris_to_piano(original_ris)
        expect = deepcopy(self.piano_single_unknown_tag)
        expect.pop("abstrackr_status")
        expect.pop("labels")
        self.check_ris_to_piano([expect], result)

    def test_ris_to_piano__with_multiple(self):
        """
        test conversion from ris to piano for multiple articles/references
        :return:
        """
        original_ris = deepcopy(self.ris_multiple)
        result = ris_to_piano(original_ris)
        self.check_ris_to_piano(self.piano_multiple, result)

    def test_piano_to_ris(self):
        """
        test conversion from piano to ris
        :return: 
        """
        original_piano = deepcopy(self.piano_single)
        result = piano_to_ris(original_piano)
        self.check_piano_to_ris(self.ris_single, result)

        # check conversion back to piano
        result = ris_to_piano(result)
        self.check_ris_to_piano([self.piano_single], result)

    def test_piano_to_ris__with_id(self):
        """
        test conversion from piano to ris with piano id
        :return:
        """
        original_piano = deepcopy(self.piano_single_id)
        result = piano_to_ris(original_piano)
        self.check_piano_to_ris(self.ris_single_id, result)

        # check conversion back to piano
        result = ris_to_piano(result)
        self.check_ris_to_piano([self.piano_single_id], result)

    def test_piano_to_ris__with_non_ris_tags(self):
        """
        test conversion from piano to ris with unknown ris tags
        :return:
        """
        original_piano = deepcopy(self.piano_single_unknown_tag)
        result = piano_to_ris(original_piano)
        self.check_piano_to_ris(self.ris_single_unknown_tag, result)

        # check conversion back to piano
        result = ris_to_piano(result)
        expect = deepcopy(self.piano_single_unknown_tag)
        expect.pop("abstrackr_status")
        expect.pop("labels")
        self.check_ris_to_piano([expect], result)

    def test_piano_to_ris__with_non_ascii(self):
        """
        test conversion from piano to ris with non ascii character(s) in articles/references
        :return: 
        """
        original_piano = deepcopy(self.piano_single_non_ascii)
        result = piano_to_ris(original_piano)
        self.check_piano_to_ris(self.ris_single_non_ascii, result)

    def test_piano_to_ris__with_multiple(self):
        """
        test conversion from piano to ris for multiple articles/references
        :return: 
        """
        original_piano = deepcopy(self.piano_multiple)
        result = piano_to_ris(original_piano)
        self.check_piano_to_ris(self.ris_multiple, result)

        # check conversion back to piano
        result = ris_to_piano(result)
        self.check_ris_to_piano(self.piano_multiple, result)

    def test_ris_to_piano_to_ris(self):
        """
        test conversion from ris to piano and back to ris
        :return: 
        """
        original_ris = deepcopy(self.ris_single)

        # convert ris to piano
        piano = ris_to_piano(original_ris)

        # convert piano back to ris
        ris = piano_to_ris(piano)

        self.check_piano_to_ris(self.ris_single, ris)

    def test_ris_to_piano_to_ris__with_non_ascii(self):
        """
        test conversion from ris to piano and back to ris with non ascii character(s)
        :return:
        """
        original_ris = deepcopy(self.ris_single_non_ascii)

        # convert ris to piano
        piano = ris_to_piano(original_ris)

        # convert piano back to ris
        ris = piano_to_ris(piano)

        self.check_piano_to_ris(self.ris_single_non_ascii, ris)

    def test_ris_to_piano_to_ris__with_multiple(self):
        """
        test conversion from ris to piano and back to ris for multiple articles/references
        :return: 
        """
        original_ris = deepcopy(self.ris_multiple)

        # convert ris to piano
        piano = ris_to_piano(original_ris)

        # convert piano back to ris
        ris = piano_to_ris(piano)

        self.check_piano_to_ris(self.ris_multiple, ris)

    def test_ris_to_piano__with_bom_character(self):
        """
        test conversion from ris to piano with BOM (Byte Order Mark) character
        :return:
        """
        ris_lg = open(os.path.join(os.path.dirname(__file__), 'resources/single_ris_article_bom_char.ris')).read()

        # convert ris to piano
        piano = ris_to_piano(ris_lg)

        self.assertEquals(len(piano), 3)

# -*- coding: utf-8 -*-
import os
import json
from unittest import TestCase
from copy import deepcopy
import xml.etree.ElementTree as ET
from piano_utils.xml_json_converter import ConversionError
from piano_utils.piano_converter import flat_json_to_piano, pubmed_to_piano, piano_to_verbose, PIANO_VERBOSE_MAPPING
from piano_utils.endnote_converter import endnote_xml_to_json
from piano_utils.pubmed_converter import pubmed_xml_to_json
from piano_utils.ris_converter import RIS_KEY_MAPPING


class TestFlatJSONToPiano(TestCase):

    @classmethod
    def setUpClass(cls):
        """
        create/setup test data
        :return:
        """
        cls.endnote_indexes = [0, 2]
        cls.pubmed_indexes = [1, 3]

        cls.ref_ids = ["e1", "", "e2", ""]
        cls.ids = [
            "1920960",              # endnote
            "26419243",             # pubmed
            "WOS:A1992HH28100022",  # endnote
            "26138797"              # pubmed
        ]
        cls.titles = [
            "Myocardial metastasis from primary lung cancer: myocardial infarction-like ECG changes and pathologic findings",
            "Post-concussion syndrome (PCS) in a youth population: defining the diagnostic value and cost-utility of brain imaging.",
            "PNEUMOCYSTIS-CARINII PNEUMONIA - AN UNCOMMON CAUSE OF DEATH IN AFRICAN PATIENTS WITH ACQUIRED-IMMUNODEFICIENCY-SYNDROME",
            "The role of the cervical spine in post-concussion syndrome."
        ]
        cls.pages = [
            "213-8",
            "2305-9",
            "617-620",
            "274-84"
        ]
        cls.authors = [
            ["Abe, S.", "Watanabe, N.", "Ogura, S.", "Kunikane, H.", "Isobe, H.",
             "Yamaguchi, E.", "Munakata, M.", "Kawakami, Y."],
            ['Morgan, Clinton D', 'Zuckerman, Scott L', 'King, Lauren E', 'Beaird, Susan E', 'Sills, Allen K',
             'Solomon, Gary S'],
            ["Abouya, Y. L.", "Beaumel, A.", "Lucas, S.", "Dagoakribi, A.", "Coulibaly, G.",
             "Ndhatz, M.", "Konan, J. B.", "Yapi, A.", "Decock, K. M."],
            ['Marshall, Cameron M', 'Vernon, Howard', 'Leddy, John J', 'Baldwin, Bradley A']
        ]
        cls.keywords = [
            'Adenocarcinoma/diagnosis/epidemiology/secondary,Adult,Aged,Aged, 80 and over,Diagnosis, Differential,*Electrocardiography,Female,Heart Neoplasms/diagnosis/epidemiology/*secondary,Humans,Incidence,*Lung Neoplasms,Male,Middle Aged,Myocardial Infarction/*diagnosis,Pericardium,Prevalence,Retrospective Studies',
            'Adolescent,Brain/*diagnostic imaging/*pathology,Child,Child, Preschool,Female,Humans,Image Processing, Computer-Assisted,Magnetic Resonance Imaging,Male,Post-Concussion Syndrome/*diagnosis,Retrospective Studies,Tomography, X-Ray Computed',
            '',
            'Adult,Brain Concussion/physiopathology,Cervical Vertebrae/*physiopathology,Female,Humans,Male,Middle Aged,Post-Concussion Syndrome/complications/*physiopathology,Whiplash Injuries/complications/physiopathology,Young Adult'
        ]

        endnote_xml = open(os.path.join(os.path.dirname(__file__),
                                            'resources/multiple_endnote_xml_articles.xml')).read()
        root = ET.fromstring(endnote_xml)
        records = root.find("records")
        for idx, record in enumerate(records):
            piano_id = ET.SubElement(record, "piano-id")
            piano_id.text = "e%s" % (idx + 1)
        endnote_xml = ET.tostring(root)
        cls.endnote_jsons = []
        endnote_json = json.loads(endnote_xml_to_json(endnote_xml))
        for i in range(len(cls.endnote_indexes)):
            cls.endnote_jsons.append(endnote_json[i])

        pubmed_xml = open(os.path.join(os.path.dirname(__file__),
                                       'resources/multiple_pubmed_xml_articles.xml')).read()
        cls.pubmed_jsons = []
        pubmed_json = json.loads(pubmed_xml_to_json(pubmed_xml))
        for i in range(len(cls.pubmed_indexes)):
            cls.pubmed_jsons.append(pubmed_json[i])

        cls.mixed_jsons = [
            cls.endnote_jsons[0],
            cls.pubmed_jsons[0],
            cls.endnote_jsons[1],
            cls.pubmed_jsons[1],
        ]

    def check_piano(self, article_idx, doc):
        """
        perform tests on a piano document against the test data
        :param article_idx: index of article in test data
        :param doc: piano document to check
        :return:
        """
        # check ref id
        self.assertEquals(self.ref_ids[article_idx], doc.get("_id", ""))

        # check id
        self.assertTrue(self.ids[article_idx] == doc.get("external_id", "") or
                        self.ids[article_idx] == doc.get("pmid", ""))

        # check title
        self.assertEquals(self.titles[article_idx], doc.get("title", ""))

        # check authors
        self.assertEquals(json.dumps(self.authors[article_idx]), doc.get("authors", ""))

        # check pages
        self.assertEquals(self.pages[article_idx], doc.get("pages", ""))

        # check keywords
        self.assertEquals(self.keywords[article_idx], doc.get("keywords", ""))

    def test_endnote_to_piano(self):
        """
        test conversion of single flattened endnote json dict to piano
        :return:
        """
        original_json = deepcopy(self.endnote_jsons[0])

        piano = flat_json_to_piano(json.dumps(original_json))
        self.assertTrue(piano)
        self.assertEquals(1, len(piano))

        self.check_piano(0, piano[0])

    def test_endnote_to_piano__with_multiple(self):
        """
        test conversion of flattened endnote json dicts to piano
        :return:
        """
        original_json = deepcopy(self.endnote_jsons)

        piano = flat_json_to_piano(json.dumps(original_json))
        self.assertTrue(piano)
        self.assertEquals(len(self.endnote_jsons), len(piano))

        for idx, doc in enumerate(piano):
            self.check_piano(self.endnote_indexes[idx], doc)

    def test_pubmed_to_piano(self):
        """
        test conversion of single flattened pubmed json dict to piano
        :return:
        """
        original_json = deepcopy(self.pubmed_jsons[0])

        piano = flat_json_to_piano(json.dumps(original_json))
        self.assertTrue(piano)
        self.assertEquals(1, len(piano))

        self.check_piano(1, piano[0])

    def test_pubmed_to_piano__with_multiple(self):
        """
        test conversion of flattened pubmed json dicts to piano
        :return:
        """
        original_json = deepcopy(self.pubmed_jsons)

        piano = flat_json_to_piano(json.dumps(original_json))
        self.assertTrue(piano)
        self.assertEquals(len(self.pubmed_jsons), len(piano))

        for idx, doc in enumerate(piano):
            self.check_piano(self.pubmed_indexes[idx], doc)

    def test_mixed_to_piano(self):
        """
        test conversion of mix of flattened endnote and piano json dicts to piano
        :return:
        """
        original_json = deepcopy(self.mixed_jsons)

        piano = flat_json_to_piano(json.dumps(original_json))
        self.assertTrue(piano)
        self.assertEquals(len(self.mixed_jsons), len(piano))

        for idx, doc in enumerate(piano):
            self.check_piano(idx, doc)

    def test_invalid_json_to_piano(self):
        """
        test conversion of invalid json to piano, should raise exception
        :return: 
        """
        invalid_json = """{
            key1: "value1",
            "key2": ["value2a", "value2b"],
            "key3": {
                "key3a": "value3a"
                "key3b": "value3b"
            }
        }"""
        self.assertRaises(ConversionError, flat_json_to_piano, invalid_json)
        self.assertRaises(ConversionError, flat_json_to_piano, "[" + invalid_json + "]")

    def test_invalid_input_to_piano(self):
        """
        test conversion of invalid input (i.e. non flat) to piano, should raise exception
        :return: 
        """
        original_json = [{
            "_id": "1",
            "key1": "value1",
            "key2": ["value2a", "value2b"],
            "key3": {
                "key3a": "value3a",
                "key3b": "value3b"
            }
        }]
        self.assertRaises(ConversionError, flat_json_to_piano, json.dumps(original_json))
        self.assertRaises(ConversionError, flat_json_to_piano, json.dumps(original_json[0]))


class TestPubMedToPiano(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.single_pubmed_xml = open(
            os.path.join(os.path.dirname(__file__), 'resources/single_pubmed_xml_article.xml')).read()
        cls.pubmed_xml_non_ascii = open(
            os.path.join(os.path.dirname(__file__), 'resources/single_pubmed_xml_article_non_ascii.xml')).read()
        cls.multiple_pubmed_xml_non_ascii = open(
            os.path.join(os.path.dirname(__file__), 'resources/multiple_pubmed_xml_articles_non_ascii.xml')).read()

        cls.single_pubmed_piano = {
            "TY": "JOUR",
            "AU": '["Morgan, Clinton D", "Zuckerman, Scott L", "King, Lauren E", "Beaird, Susan E", "Sills, Allen K", "Solomon, Gary S"]',
            "AB": """PURPOSE: Approximately 90% of concussions are transient, with symptoms resolving within 10-14 days. However, a minority of patients remain symptomatic several months post-injury, a condition known as post-concussion syndrome (PCS). The treatment of these patients can be challenging. The goal of our study was to assess the utility and cost-effectiveness of neurologic imaging two or more weeks post-injury in a cohort of youth with PCS. METHODS: We conducted a retrospective study of 52 pediatric patients with persistent post-concussion symptoms after 3 months. We collected demographics and neuroimaging results obtained greater than 2 weeks post-concussion. Neuroimaging ordered in the first 2 weeks post-concussion was excluded, except to determine the rate of re-imaging. Descriptive statistics and corresponding cost data were collected. RESULTS: Of 52 patients with PCS, 23/52 (44%) had neuroimaging at least 2 weeks after the initial injury, for a total of 32 diagnostic studies. In summary, 1/19 MRIs (5.3%), 1/8 CTs (13%), and 0/5 x-rays (0%) yielded significant positive findings, none of which altered clinical management. Chronic phase neuroimaging estimated costs from these 52 pediatric patients totaled $129,025. We estimate the cost to identify a single positive finding was $21,000 for head CT and $104,500 for brain MRI. CONCLUSIONS: In this cohort of pediatric PCS patients, brain imaging in the chronic phase (defined as more than 2 weeks after concussion) was pursued in almost half the study sample, had low diagnostic yield, and had poor cost-effectiveness. Based on these results, outpatient management of pediatric patients with long-term post-concussive symptoms should rarely include repeat neuroimaging beyond the acute phase.""",
            "CY": "Germany",
            "DA": "2015/09/29",
            "DB": "PubMed",
            "DO": "10.1007/s00381-015-2916-y",
            "ID": "26419243",
            "IS": "12",
            "KW": '["Brain injury", "Computed tomography (CT) neuroimaging", "Concussion", "Decision making", "Magnetic resonance imaging (MRI)", "Mild traumatic brain injury", "Post-concussion syndrome", "Sports"]',
            "LA": "eng",
            "PY": "2015",
            "SN": "1433-0350",
            "SP": "2305-9",
            "TI": "Post-concussion syndrome (PCS) in a youth population: defining the diagnostic value and cost-utility of brain imaging.",
            "VL": "31",
        }

        cls.single_pubmed_piano_non_ascii = {
            "TY": "JOUR",
            "AU": '["Marshall, Cameron M", "Vernon, Howard", "Leddy, John J", "Baldwin, Bradley A"]',
            "AB": "While much is known regarding the pathophysiology surrounding concussion injuries in the acute phase, there is little evidence to support many of the theorized etiologies to post-concussion syndrome (PCS); the chronic phase of concussion occurring in ∼ 10-15% of concussed patients. This paper reviews the existing literature surrounding the numerous proposed theories of PCS and introduces another potential, and very treatable, cause of this chronic condition; cervical spine dysfunction due to concomitant whiplash-type injury. We also discuss a short case-series of five patients with diagnosed PCS having very favorable outcomes following various treatment and rehabilitative techniques aimed at restoring cervical spine function.".decode("utf-8"),
            "CY": "England",
            "DA": "2015/07/03",
            "DB": "PubMed",
            "DO": "10.1080/00913847.2015.1064301",
            "ID": "26138797",
            "IS": "3",
            "KW": '["Brain concussion", "musculoskeletal manipulations", "pathophysiology", "post-concussion syndrome", "whiplash injuries"]',
            "LA": "eng",
            "PY": "2015",
            "SN": "2326-3660",
            "SP": "274-84",
            "TI": "The role of the cervical spine in post-concussion syndrome.",
            "VL": "43",
        }

        cls.multiple_pubmed_piano = [
            cls.single_pubmed_piano,
            cls.single_pubmed_piano_non_ascii,
            cls.single_pubmed_piano_non_ascii
        ]

    def test_pubmed_to_piano(self):
        """
        test conversion of pubmed xml string to piano
        :return:
        """
        result = pubmed_to_piano(self.single_pubmed_xml)
        self.assertTrue(result)
        self.assertTrue(isinstance(result, list))
        result = result[0]
        for k, v in self.single_pubmed_piano.iteritems():
            self.assertTrue(k in result, "Result is missing %s" % k)
            self.assertEquals(v, result[k])

    def test_pubmed_to_piano__with_non_ascii(self):
        """
        test conversion of pubmed xml to piano with non ascii character/s
        :return:
        """
        result = pubmed_to_piano(self.pubmed_xml_non_ascii)
        self.assertTrue(result)
        self.assertTrue(isinstance(result, list))
        result = result[0]
        for k, v in self.single_pubmed_piano_non_ascii.iteritems():
            self.assertTrue(k in result, "Result is missing %s" % k)
            self.assertEquals(v, result[k])

    def test_pubmed_to_piano__with_multiple(self):
        """
        test conversion of pubmed xml to piano with multiple articles
        :return:
        """
        result = pubmed_to_piano(self.multiple_pubmed_xml_non_ascii)
        self.assertTrue(result)
        self.assertTrue(isinstance(result, list))
        for idx, i in enumerate(self.multiple_pubmed_piano):
            for k, v in i.iteritems():
                self.assertTrue(k in result[idx], "Result %s is missing %s" % (idx, k))
                self.assertEquals(v, result[idx][k])


class TestPianoToVerbose(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.piano_single = {
            "TY": "THES",
            "AU": '["Beh, Thian Thian"]',
            "Y2": "2017/01/04",
            "Y1": "2016",
            "UR": "http://hdl.handle.net/11343/123144",
            "AB": "The centromere plays a crucial role in genome inheritance - ensuring proper segregation of sister chromatids into each daughter cell. In cancer, especially in later stages of solid tumours, cells are often presented with extensive chromosomal abnormalities. However, the involvement of defective centromeres in the disease formation and progression has not been carefully studied. Hence, my PhD project aimed to investigate the role of defective centromeres in cancer progression using the NCI-60 panel of cancer cell lines. The spectrum of centromere-related abnormalities were first characterised with pan-centromeric FISH probes and then with the addition of CENP-A immunofluorescence. For HOP-92 and SN12C, cell lines showing high prevalence of functional dicentric chromosomes, expansion of single cell clones from the initial heterogeneous population was carried out to further study the involvement of dicentric chromosomes in cancer genomic instability. Neocentromere formation sites in cancer cell lines were investigated using cell lines with high prevalence of neocentric chromosomes. A neocentromere was found in T-47D which marked the first report of a neocentromere discovered in a long-established and widely used cell line, and also the first neocentromere to be reported in breast cancer. Separately, an antibody screen to identify antibodies recognising components of an active centromere in methanol-acetic acid stored cells was performed because thus far, antibodies that are widely used for functional centromere detection mainly worked on freshly harvested cells whereas most cytogenetic samples are stored long-term in methanol-acetic acid fixative. I found a commercially available rabbit monoclonal anti-CENP-C that worked on fixed samples and in addition, I combined three methods (FISH, immunofluorescence and mFISH) to obtain more information from the same metaphase spread. I then proceeded to test anti-CENP-C in my method, CENP-IF-cenFISH-mFISH, on dicentric- and neocentric-containing cancer cell lines and proposed other utilities for this method which I have developed.",
            "KW": '["centromere", "CENP-A", "CENP-C", "immunofluorescence", "fluorescence in situ hybridization (FISH)", "multicolour FISH (mFISH)", "neocentromere", "dicentric", "NCI-60", "cancer", "cytogenetics"]',
            "T1": "The role of centromere defects in cancer formation and progression",
            "L1": "/bitstream/handle/11343/123144/2016%20Dec%20Thian%20Beh%20-%20Thesis.pdf?sequence=1&isAllowed=n",
        }
        cls.piano_single_id = deepcopy(cls.piano_single)
        cls.piano_single_id["_id"] = "5a8c262b1dae1e0034513fa5"
        cls.piano_single_non_ascii = {
            "TY": "JOUR",
            "AU": '["Shannon,Claude E."]',
            "PY": "1948/07//",
            "TI": "A Mathematical Theory of Communication",
            "JF": "Bell System Technical Journal",
            "AB": "This is a test abstract with non-ascii character; here — it is! And another [∴] one.".decode(
                "utf-8"),
            "SP": "379",
            "EP": "423",
            "VL": "27",
        }
        cls.piano_single_unknown_tag = {
            "TY": "Clinical Trial",
            "AU": '["Shannon,Claude E."]',
            "PY": "1948/07//",
            "TI": "A Mathematical Theory of Communication",
            "JF": "Bell System Technical Journal",
            "SP": "379",
            "EA": "evidently 1 evidently 2",
            "EP": "423",
            "VL": "27",
            "JP": "CRISPR",
            "DC": "Direct Current",
            "MV": "unknown tag",
        }
        cls.piano_multiple = [
            cls.piano_single,
            {
                "TY": "RPRT",
                "T1": "Rearrangement hotspots in the coding and non-coding genome drive breast carcinogenesis",
                "T2": "A somatic-mutational process recurrently duplicates germline susceptibility loci and tissue-specific super-enhancers in breast cancers",
                "A1": '["Glodzik, Dominik", "Morganella, Sandro", "Davies, Helen", "Simpson, Peter", "Li, Yilong", "Zou, Xueqing", "DiezPerez, Javier", "Staaf, Johan", "Alexandrov, Ludmil B.", "Smid, Marcel", "Brinkman, Arie", "Rye, Inga", "Russnes, Hege", "Raine, Keiran", "Purdie, Colin", "Lakhani, Sunil", "Thompson, Alastair", "Birney, Ewan", "Stunnenberg, Hendrik", "Vijver, Marc", "Martens, John", "BorresenDale, AnneLise", "Richardson, Andrea", "Kong, Gu", "Viari, Alain", "Easton, Douglas", "Evan, Gerard", "Campbell, Peter", "Stratton, Michael", "NikZainal, Serena"]',
                "AD": "Wellcome Trust Sanger Institute, Hinxton, Cambridge CB10 1SA, UK",
                "N1": '["Published as: A somatic-mutational process recurrently duplicates germline susceptibility loci and tissue-specific super-enhancers in breast cancers. Nature Genetics ; Vol.49, iss.3, p.341-348 ; January 23, 2017", "Dataset: RASSTI"]',
                "Y1": "2016-03-21",
                "SP": "30 p.",
                "M3": "Basic Biological Sciences(59) ; Biological Science",
                "SN": "LA-UR-16-21897",
                "DO": "10.1038/ng.3771",
                "UR": "http://permalink.lanl.gov/object/view?what=info:lanl-repo/lareport/LA-UR-16-21897",
            },
            {
                "TY": "JOUR",
                "ID": "12345",
                "T1": "Title of reference",
                "A1": '["Marx, Karl", "Lindgren, Astrid"]',
                "A2": '["Glattauer, Daniel"]',
                "Y1": "2014//",
                "N2": "BACKGROUND: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  RESULTS: Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. CONCLUSIONS: Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.",
                "KW": '["Pippi", "Nordwind", "Piraten"]',
                "JF": "Lorem",
                "JA": "lorem",
                "VL": "9",
                "IS": "3",
                "SP": "e0815",
                "CY": "United States",
                "PB": "Fun Factory",
                "SN": "1932-6208",
                "M1": "1008150341",
                "L2": "http://example.com",
                "UR": "http://example_url.com",
            },
            {
                "TY": "BOOK",
                "ID": "122725",
                "T1": "La survenue du cancer : effets de court et moyen termes sur l’emploi, le chômage et les arrêts maladie.".decode(
                    "utf-8"),
                "A1": '["BARNAY T.", "BEN HALIMA M.A.", "DUGUET E.", "LANFRANCHI J.", "LE CLAINCHE C."]',
                "PY": "2015/04",
                "ED": "Institut de Recherche et Documentation en Economie de la Santé. (I.R.D.E.S.). Paris. FRA".decode(
                    "utf-8"),
                "KW": '["CHOMAGE", "FRANCE", "EMPLOI", "ETAT SANTE", "AGE", "INDEMNITE JOURNALIERE", "MALADIE LONGUE DUREE", "INEGALITE DEVANT SOINS", "ARRET TRAVAIL", "IMPACT", "SEXE", "CANCER", "CONGE MALADIE", "MOYEN TERME", "GRAVITE", "LONG TERME", "ABSENTEISME"]',
                "AB": "La réduction des inégalités face à la maladie est un des attendus majeurs du troisième Plan cancer 2014-2019 qui préconise de « diminuer l’impact du cancer sur la vie personnelle » afin d’éviter la « double peine » (maladie et exclusion du marché du travail). Dans ce contexte, nous évaluons l’impact de un à cinq ans d’un primo-enregistrement en Affection de longue durée (ALD) caractérisant le cancer sur la situation professionnelle et la durée passée en emploi, maladie et chômage de salariés du secteur privé. Nous utilisons la base de données administratives Hygie, recensant la carrière professionnelle et les épisodes de maladie d’un échantillon de salariés affiliés au Régime général de la Sécurité sociale. L’évaluation de l’impact de la survenue du cancer s’appuie sur une méthode de double différence avec appariement exact pour comparer les salariés malades aux salariés sans aucune ALD. La première année après le diagnostic correspond au temps des traitements caractérisé par une augmentation du nombre de trimestres d’arrêts de travail pour maladie de 1,7 pour les femmes et de 1,2 pour les hommes. L’âge joue également un rôle sur les absences liées à la maladie. Par ailleurs, l’employabilité des travailleurs atteints du cancer diminue avec le temps. La proportion de femmes et d’hommes employés au moins un trimestre, baisse respectivement de 8 et 7 points de pourcentage dans l’année suivant la survenue du cancer et jusqu’à treize points de pourcentage cinq ans plus tard. Cette distance à l’emploi se renforce lorsque les salariés malades sont plus âgés. L’effet de la maladie à cinq ans est respectivement de 15 et 19 points de pourcentage pour les hommes de plus de 51 ans et pour les femmes de plus de 48 ans. Ces différences de genre et d’âge peuvent traduire des différences de localisation et de sévérité des cancers, d’une part, de séquelles des cancers et de difficultés de réinsertion sur le marché du travail plus importantes avec l’avancée en âge, d’autre part.".decode(
                    "utf-8"),
                "UR": "http://www.irdes.fr/recherche/documents-de-travail/065-la-survenue-du-cancer-effets-de-court-et-moyen-termes-sur-emploi-chomage-arrets-maladie.pdf",
                "N1": '["DT65"]',
                "DP": "IRDES",
                "LA": "FRE",
                "PB": "Paris : Irdes",
                "EP": "44p.",
                "T3": "Document de travail Irdes ; 65",
            }
        ]

    def check_piano_to_verbose(self, expect, actual):
        reverse_mapping = {v: k for k, v in PIANO_VERBOSE_MAPPING.iteritems() if v != "_id"}
        for idx, row in enumerate(actual):
            reverse = {}
            for k, v in row.iteritems():
                reverse[reverse_mapping.get(k, k)] = v
            self.assertDictEqual(expect[idx], reverse)

    def test_piano_to_verbose(self):
        """
        test conversion from piano to verbose piano
        :return:
        """
        original_piano = deepcopy(self.piano_single)
        result = piano_to_verbose(original_piano)
        self.check_piano_to_verbose([original_piano], result)

    def test_piano_to_verbose__with_id(self):
        """
        test conversion from piano to verbose piano with piano id
        :return:
        """
        original_piano = deepcopy(self.piano_single_id)
        result = piano_to_verbose(original_piano)
        self.check_piano_to_verbose([original_piano], result)

    def test_piano_to_verbose__with_all_fields(self):
        """
        test conversion from piano to verbose piano with all fields
        :return:
        """
        original_piano = deepcopy(RIS_KEY_MAPPING)
        [original_piano.pop(k) for k in ["CP", "UK", "ER"]]
        original_piano["_id"] = original_piano.pop("PI")
        result = piano_to_verbose(original_piano)
        self.check_piano_to_verbose([original_piano], result)

    def test_piano_to_verbose__with_non_ris_tag(self):
        """
        test conversion from piano to verbose piano with non ris tag
        :return:
        """
        original_piano = [deepcopy(self.piano_single_unknown_tag)]
        result = piano_to_verbose(original_piano)
        self.check_piano_to_verbose(original_piano, result)

    def test_piano_to_verbose__with_non_ascii(self):
        """
        test conversion from piano to verbose piano with non ascii character(s)
        :return:
        """
        original_piano = [deepcopy(self.piano_single_non_ascii)]
        result = piano_to_verbose(original_piano)
        self.check_piano_to_verbose(original_piano, result)

    def test_piano_to_verbose__with_multiple(self):
        """
        test conversion from piano to verbose piano with multiple articles/references
        :return:
        """
        original_piano = deepcopy(self.piano_multiple)
        result = piano_to_verbose(original_piano)
        self.check_piano_to_verbose(original_piano, result)

import json
from types import NoneType
from bs4 import Tag
from collections import OrderedDict
from piano_utils.utils.parse_xml import Mapper, XmlToJson, XmlToString
from xml_json_converter import xml_to_json, json_to_xml
from RISparser.config import LIST_TYPE_TAGS


def pubmed_xml_to_json(xml_string):
    """
    Converts a PubMed XML string to a flattened JSON structure
    :param xml_string: PubMed XML string to flatten into JSON
    :return: JSON
    """
    return xml_to_json(xml_string, element_name="PubmedArticle")


def json_to_pubmed_xml(json_string):
    """
    Converts a flattened PubMed JSON string into a PubMed XML string
    :param json_string: flattened PubMed JSON string to convert to XML
    :return: XML
    """
    return json_to_xml(json_string, "PubmedArticleSet", "PubmedArticle")


def pubmed_xml_to_piano(xml_string):
    """
    Converts a PubMed XML string to a list of Piano dictionaries
    :param xml_string: Pubmed XML string to convert into a list of Piano dictionaries
    :return: list
    """
    parser = XmlToJson(PubMedMapper())
    return parser.xml_to_json(xml_string)


def pubmed_json_to_piano(json_string):
    """
    Converts a flattened PubMed JSON string into a Piano dictionary
    :param json_string: flattened PubMed JSON string to convert into a Piano dictionary
    :return: dict
    """
    def get_non_pubmed_dict(d):
        non_pubmed_dict = {}
        for k in d.keys():
            if not k.startswith(("MedlineCitation", "PubmedData")):
                non_pubmed_dict[k] = d.pop(k)
        return non_pubmed_dict

    json_dict = json.loads(json_string)
    non_pubmed_data = []
    if isinstance(json_dict, list):
        for article in json_dict:
            non_pubmed_data.append(get_non_pubmed_dict(article))
    else:
        non_pubmed_data.append(get_non_pubmed_dict(json_dict))

    piano_dict = pubmed_xml_to_piano(json_to_pubmed_xml(json.dumps(json_dict)))
    if non_pubmed_data:
        for idx, _ in enumerate(non_pubmed_data):
            piano_dict[idx].update(non_pubmed_data[idx])

    return piano_dict


def pubmed_to_ris(xml_string):
    """
    Converts a PubMed XML string to RIS
    :param xml_string: PubMed XML string to convert
    :return: string
    """
    parser = XmlToString(PubMedRISMapper())
    return parser.xml_to_string(xml_string)


class PubMedRISMapper(Mapper):

    def __init__(self):
        super(PubMedRISMapper, self).__init__()
        self.element_names = "PubmedArticle"

    def attach_article_ids(self, xml_items):
        return [(i, {}) for i in xml_items]

    def transform_to_piano(self, xml_soup_article):
        # TODO implement

        article = OrderedDict()

        # ref type
        article["TY"] = self.get_article_type(xml_soup_article)
        # authors
        article['AU'] = self.get_authors(xml_soup_article)
        # abstract
        article['AB'] = self.get_abstract(xml_soup_article)
        # place published
        article['CY'] = self.get_text_if_not_null(xml_soup_article.find('Country'))
        # date
        ad = None
        y, m, d = self.get_date(xml_soup_article, 'ArticleDate')
        if y is not None and m is not None and d is not None:
            ad = "/".join([y, m, d])
        elif y is not None:
            ad = y
        article["DA"] = ad
        # database
        article["DB"] = "PubMed"
        # doi
        article["DO"] = self.get_doi(xml_soup_article)
        # id
        article["ID"] = self.get_text_if_not_null(xml_soup_article.select_one('MedlineCitation > PMID'))
        # issue
        article["IS"] = self.get_text_if_not_null(xml_soup_article.find('Issue'))
        # keywords
        article["KW"] = [self.get_text_if_not_null(_xml) for _xml in xml_soup_article.select('KeywordList > Keyword')]
        # language
        article["LA"] = self.get_text_if_not_null(xml_soup_article.find('Language'))
        # notes
        article["N1"] = self.get_general_notes(xml_soup_article)
        # published date
        pd = None
        y, m, d = self.get_date(xml_soup_article, 'PubDate')
        if y is not None and m is not None and d is not None:
            pd = "/".join([y, m, d])
        elif y is not None:
            pd = y
        article["PY"] = pd
        # isbn/issn
        article["SN"] = self.get_text_if_not_null(xml_soup_article.find('ISSN'))
        # start page
        article["SP"] = self.get_text_if_not_null(xml_soup_article.select_one('Pagination > MedlinePgn'))
        # titles
        orig_title = self.get_text_if_not_null(xml_soup_article.find('VernacularTitle'))
        title = self.get_text_if_not_null(xml_soup_article.find('ArticleTitle'))
        if orig_title:
            article["TI"] = orig_title
            article["TT"] = title
        else:
            article["TI"] = title
        article["T2"] = self.get_text_if_not_null(xml_soup_article.select_one('Journal > Title'))
        # volume
        article["VL"] = self.get_text_if_not_null(xml_soup_article.select_one('Volume'))

        ris = ""
        for k in article.keys():
            if article[k] is None:
                continue
            if k in LIST_TYPE_TAGS:
                for v in article[k]:
                    ris += "%s  - %s\n" % (k, v)
            else:
                ris += "%s  - %s\n" % (k, article[k])
        if ris:
            ris += "ER  - "

        return ris

    def get_qualifier_name(self, qualifier_xml):
        """
        Gets as an input full xml qualifier: <QualifierName MajorTopicYN="Y" UI="Q000379">methods</QualifierName>
        :param qualifier_xml:
        :return:
        """
        if qualifier_xml is None:
            return None

        text_ = self.get_text_if_not_null(qualifier_xml)

        if isinstance(qualifier_xml, Tag) \
                and qualifier_xml.has_attr('MajorTopicYN') and qualifier_xml['MajorTopicYN'] == "Y":
            return '*' + text_
        else:
            return text_

    def get_single_mesh_heading(self, _xml):

        text_ = [self.get_text_if_not_null(_xml.select_one('DescriptorName'))] + \
                [self.get_qualifier_name(qualifier_xml)
                 if qualifier_xml is not NoneType else None
                 for qualifier_xml in _xml.find_all("QualifierName")]

        # skip all None
        text_ = filter(lambda t: t is not None, text_)

        str_mesh_heading = "/".join(text_)

        return str_mesh_heading

    def get_article_type(self, xml):
        pubmed_to_ris_reference_type_mapping = {
            "Classical Article": "CLSWK",
            "Dictionary": "DICT",
            "Government Publications": "GOVDOC",
            "Journal Article": "JOUR",
            "Newspaper Article": "NEWS",
            "Video-Audio Media": "VIDEO",
        }
        type_elements = xml.select('PublicationTypeList > PublicationType')
        types = []
        for t in type_elements:
            type = self.get_text_if_not_null(t)
            if type in pubmed_to_ris_reference_type_mapping:
                return pubmed_to_ris_reference_type_mapping[type]
            types.append(type)
        return types[0] if types else "GEN"

    def get_authors(self, _xml):
        # extract elements
        author_elements = _xml.select('AuthorList > Author')
        authors = []
        for a in author_elements:
            if isinstance(a, Tag) and a.has_attr("ValidYN") and a["ValidYN"] == "Y":
                last = self.get_text_if_not_null(a.select_one("LastName"))
                first = self.get_text_if_not_null(a.select_one("ForeName"))
                if last and first:
                    authors.append("%s, %s" % (last, first))
                elif last:
                    authors.append(last)

        return authors

    def get_abstract(self, _xml):
        abstracts_soup = _xml.select('Abstract > AbstractText')
        if not abstracts_soup:
            return None
        abstracts = []
        for abstract in abstracts_soup:
            label = abstract.get("Label", "")
            if label:
                label += ": "
            text = self.get_text_if_not_null(abstract)
            if text:
                abstracts.append(label + text)
        return u"\n\n".join(abstracts) if abstracts else None

    def get_doi(self, xml):
        if xml is None:
            return None

        doi = None
        for _id in xml.select("ELocationID"):
            text = self.get_text_if_not_null(_id)
            if isinstance(_id, Tag):
                if _id.has_attr('EIdType') and _id['EIdType'] == "doi":
                    if _id.has_attr('ValidYN') and _id['ValidYN'] == "Y":
                        return text
                    doi = text
        return doi

    def get_date(self, xml, name):
        y = self.get_text_if_not_null(xml.select_one("%s > Year" % name))
        m = self.get_text_if_not_null(xml.select_one("%s > Month" % name))
        d = self.get_text_if_not_null(xml.select_one("%s > Day" % name))

        return y, m, d

    def get_general_notes(self, xml):
        notes = []
        for n in xml.select("GeneralNote"):
            if self.get_text_if_not_null(n) is not None:
                notes.append(n)
        return notes


class PubMedMapper(Mapper):

    def __init__(self):
        super(PubMedMapper, self).__init__()
        self.element_names = "PubmedArticle"

    def attach_article_ids(self, xml_items):
        return [(i, {"pmid": self.get_text_if_not_null(i.select_one('PMID'))}) for i in xml_items]

    def transform_to_piano(self, xml_soup_article):
        article = {}

        article['title'] = self.get_text_if_not_null(xml_soup_article.find('ArticleTitle'))
        article['doi'] = self.get_text_if_not_null(
            xml_soup_article.select_one('ArticleIdList > ArticleId[IdType="doi"]'))
        article['journal'] = self.get_text_if_not_null(xml_soup_article.select_one('Journal > Title'))
        article['published_date'] = self.get_text_if_not_null(xml_soup_article.select_one('PubDate > MedlineDate'))
        article['authors'] = json.dumps(
            [self.get_author(_xml) for _xml in xml_soup_article.select('AuthorList > Author')])
        article['abstract'] = self.get_abstract(xml_soup_article)
        article['keywords'] = ",".join(
            [self.get_single_mesh_heading(_xml) for _xml in xml_soup_article.select('MeshHeadingList > MeshHeading')])
        article['pmid'] = self.get_text_if_not_null(xml_soup_article.select_one('MedlineCitation > PMID'))
        if article['pmid']:
            article['external_id'] = "PUBMED:" + article['pmid']
        # article['pubmed_xml'] = self.get_text_if_not_null(xml_soup_article)

        article["author_address"] = self.get_text_if_not_null(
            xml_soup_article.select_one('AuthorList > Author > AffiliationInfo > Affiliation'))
        article["pages"] = self.get_text_if_not_null(
            xml_soup_article.select_one('Article > Pagination > MedlinePgn'))
        article["volume"] = self.get_text_if_not_null(
            xml_soup_article.select_one('Journal > JournalIssue > Volume'))
        article["number"] = self.get_text_if_not_null(
            xml_soup_article.select_one('Journal > JournalIssue > Issue'))

        pubdate_year = self.get_text_if_not_null(xml_soup_article.select_one('PubMedPubDate[PubStatus="pubmed"] > Year'))
        pubdate_month = self.get_text_if_not_null(xml_soup_article.select_one('PubMedPubDate[PubStatus="pubmed"] > Month'))
        pubdate_day = self.get_text_if_not_null(xml_soup_article.select_one('PubMedPubDate[PubStatus="pubmed"] > Day'))
        if pubdate_day and pubdate_month and pubdate_year:
            article["edition"] = pubdate_year + "/" + pubdate_month + "/" + pubdate_day

        article["isbn"] = self.get_text_if_not_null(xml_soup_article.select_one('Journal > ISSN'))
        article["language"] = self.get_text_if_not_null(xml_soup_article.select_one('Article > Language'))

        # article["pubmed_xml"] = xml_soup_article.PubmedArticle.wrap(xml_soup_article.new_tag("PubmedArticleSet"))

        return article

    def get_qualifier_name(self, qualifier_xml):
        """
        Gets as an input full xml qualifier: <QualifierName MajorTopicYN="Y" UI="Q000379">methods</QualifierName>
        :param qualifier_xml:
        :return:
        """
        if qualifier_xml is None:
            return None

        text_ = self.get_text_if_not_null(qualifier_xml)

        if isinstance(qualifier_xml, Tag) \
                and qualifier_xml.has_attr('MajorTopicYN') and qualifier_xml['MajorTopicYN'] == "Y":
            return '*' + text_
        else:
            return text_

    def get_single_mesh_heading(self, _xml):

        text_ = [self.get_text_if_not_null(_xml.select_one('DescriptorName'))] + \
                [self.get_qualifier_name(qualifier_xml)
                 if qualifier_xml is not NoneType else None
                 for qualifier_xml in _xml.find_all("QualifierName")]

        # skip all None
        text_ = filter(lambda t: t is not None, text_)

        str_mesh_heading = "/".join(text_)

        return str_mesh_heading

    def get_author(self, _xml):
        # extract elements
        text_ = [_xml.select_one('LastName'), _xml.select_one('ForeName')]

        # skip None values and join by comma
        return u", ".join(i.get_text() for i in filter(lambda t: not isinstance(t, NoneType), text_))

    def get_abstract(self, _xml):
        abstracts_soup = _xml.select('Abstract > AbstractText')
        if not abstracts_soup:
            return None
        abstracts = []
        for abstract in abstracts_soup:
            label = abstract.get("Label", "")
            if label:
                label += ": "
            text = self.get_text_if_not_null(abstract)
            if text:
                abstracts.append(label + text)
        return u"\n\n".join(abstracts) if abstracts else None

import json
from xml_json_converter import ConversionError
from pubmed_converter import pubmed_json_to_piano, pubmed_to_ris
from endnote_converter import endnote_json_to_piano
from ris_converter import ris_to_piano, piano_to_ris
from csv_converter import csv_to_piano


PIANO_VERBOSE_MAPPING = {
    "TY": "type",
    "A1": "authors_first",
    "A2": "authors_second",
    "A3": "authors_tertiary",
    "A4": "authors_subsidiary",
    "AB": "abstract",
    "AD": "author_address",
    "AN": "accession_number",
    "AU": "authors",
    "AV": "archive_location",
    "BT": "title_book",
    "C1": "custom_1",
    "C2": "custom_2",
    "C3": "custom_3",
    "C4": "custom_4",
    "C5": "custom_5",
    "C6": "custom_6",
    "C7": "custom_7",
    "C8": "custom_8",
    "CA": "caption",
    "CN": "call_number",
    "CT": "title_unpublished",
    "CY": "place_published",
    "DA": "date",
    "DB": "database_name",
    "DO": "doi",
    "DP": "database_provider",
    "ED": "editor",
    "EP": "end_page",
    "ET": "edition",
    "ID": "reference_id",
    "IS": "issue_number",
    "J1": "periodical_name_user",
    "J2": "title_alternate",
    "JA": "periodical_name_standard",
    "JF": "journal_name_1",
    "JO": "journal_name_2",
    "KW": "keywords",
    "L1": "pdf_link",
    "L2": "fulltext_link",
    "L3": "related_records",
    "L4": "images",
    "LA": "language",
    "LB": "label",
    "LK": "website_link",
    "M1": "number",
    "M2": "miscellaneous_2",
    "M3": "type_of_work",
    "N1": "notes",
    "N2": "abstract_2",
    "NV": "number_of_volumes",
    "OP": "original_publication",
    "PB": "publisher",
    "PI": "_id",
    "PP": "publishing_place",
    "PY": "publication_year",
    "RI": "reviewed_item",
    "RN": "research_notes",
    "RP": "reprint_edition",
    "SE": "section",
    "SN": "isbn",
    "SP": "start_page",
    "ST": "title_short",
    "T1": "title_primary",
    "T2": "journal_title",
    "T3": "tertiary_title",
    "TA": "author_translated",
    "TI": "title",
    "TT": "title_translated",
    "U1": "user_definable_1",
    "U2": "user_definable_2",
    "U3": "user_definable_3",
    "U4": "user_definable_4",
    "U5": "user_definable_5",
    "UR": "url",
    "VL": "volume_number",
    "VO": "published_standard_number",
    "Y1": "primary_date",
    "Y2": "access_date",
}


def flat_json_to_piano(json_string):
    """
    Converts flattened json to Piano structure
    :param json_string: flattened JSON string to convert into a piano dictionary
    :return:
    """
    try:
        json_value = json.loads(json_string)
    except ValueError as e:
        raise ConversionError("Invalid JSON string: %s" % e.message)

    try:
        if isinstance(json_value, list):
            if len(json_value) > 1:
                json_dicts = []
                for j in json_value:
                    if "MedlineCitation_PMID_#text" in j:
                        piano = pubmed_json_to_piano(json.dumps(j))
                    else:
                        piano = endnote_json_to_piano(json.dumps(j))
                    if piano:
                        json_dicts += piano
                return json_dicts
            else:
                json_value = json_value[0]

        if isinstance(json_value, dict):
            if "MedlineCitation_PMID_#text" in json_value:
                return pubmed_json_to_piano(json.dumps(json_value))
            else:
                return endnote_json_to_piano(json.dumps(json_value))
    except Exception as e:
        raise ConversionError(e.message)


def pubmed_to_piano(xml_string):
    """
    Converts PubMed XML to Piano structure
    :param xml_string: PubMed XML string to convert
    :return: 
    """
    try:
        return ris_to_piano(pubmed_to_ris(xml_string))
    except Exception as e:
        raise ConversionError(e.message)


def piano_to_verbose(piano):
    """
    Converts a list of Piano dictionaries to a list of dictionaries with full (readable/verbose) key names
    :param piano: list/dict of piano articles/references to convert
    :return:
    """
    if isinstance(piano, dict):
        piano = [piano]

    if not isinstance(piano, list):
        return []

    try:
        return [{PIANO_VERBOSE_MAPPING.get(k, k): v for k, v in row.items()} for row in piano]
    except Exception as e:
        raise ConversionError(e.message)

import json
from bs4 import Tag
import xml.etree.ElementTree as ET
from xml_json_converter import xml_to_json, json_to_xml
from piano_utils.utils.parse_xml import Mapper, XmlToJson, XmlToString
from collections import OrderedDict
from RISparser.config import LIST_TYPE_TAGS
from copy import deepcopy
from lxml.etree import Element, SubElement, tostring


def endnote_xml_to_json(xml_string):
    """
    Converts an EndNote XML string to a flattened JSON structure
    :param xml_string: EndNote XML string to flatten into JSON
    :return: JSON
    """
    return xml_to_json(xml_string, element_name="record")


def json_to_endnote_xml(json_string):
    """
    Converts a flattened EndNote JSON string into a EndNote XML string
    :param json_string: flattened EndNote JSON string to convert to XML
    :return: XML
    """
    json_value = json.loads(json_string)
    if isinstance(json_value, list):
        for j in json_value:
            if "_id" in j:
                j["piano-id"] = j.pop("_id")
    else:
        if "_id" in json_value:
            json_value["piano-id"] = json_value.pop("_id")
    return json_to_xml(json.dumps(json_value), ["xml", "records"], "record")


def endnote_xml_to_piano(xml_string):
    """
    Converts an EndNote XML string to a list of Piano dictionaries
    :param xml_string: EndNote XML string to convert into a list of Piano dictionaries
    :return: list
    """
    parser = XmlToJson(EndNoteMapper())
    return parser.xml_to_json(xml_string)


def endnote_json_to_piano(json_string):
    """
    Converts a flattened EndNote JSON string into a Piano dictionary
    :param json_string: flattened EndNote JSON string to convert into a piano dictionary
    :return: dict
    """
    piano_dict = endnote_xml_to_piano(json_to_endnote_xml(json_string))
    return piano_dict


def endnote_to_ris(xml_string):
    """
    Converts an EndNote XML string to RIS
    :param xml_string: EndNote XML to convert
    :return: string
    """
    parser = XmlToString(EndNoteRISMapper())
    return parser.xml_to_string(xml_string)


def piano_to_endnote(piano):
    """
    Converts a piano string to EndNote XML
    :param piano: piano dictionary/list to convert
    :return: xml
    """
    def append_with_style(parent, name, value):
        e = SubElement(parent, name)
        style = SubElement(e, 'style', {'face': 'normal', 'font': 'default', 'size': '100%'})
        style.text = value

    piano = deepcopy(piano)
    if isinstance(piano, dict):
        piano = [piano]

    root = Element('xml')
    records = Element('records')
    for p in piano:
        record = Element('record')

        source_app = SubElement(record, 'source-app', {'name': 'EndNote'})
        source_app.text = "EndNote"

        # id
        if "_id" in p:
            element = SubElement(record, 'piano-id')
            element.text = p.pop("_id")
        elif "ID" in p:
            element = SubElement(record, 'piano-id')
            element.text = p.pop("ID")

        if "TY" in p:
            record.append(Element('ref-type', {'name': p.pop('TY')}))

        # abstract
        if "AB" in p:
            append_with_style(record, 'abstract', p.pop("AB"))
        elif "N2" in p:
            append_with_style(record, 'abstract', p.pop("N2"))

        # author address
        if "AD" in p:
            append_with_style(record, 'auth-address', p.pop("AD"))

        # accession number
        if "AN" in p:
            append_with_style(record, 'accession-num', p.pop("AN"))

        # authors
        if "AU" in p or "A1" in p:
            if "AU" in p:
                author_list = p.pop("AU")
            else:
                author_list = p.pop("A1")
            try:
                author_list = json.loads(author_list)
            except ValueError:
                pass
            contributors = SubElement(record, 'contributors')
            authors = SubElement(contributors, 'authors')
            for a in author_list:
                append_with_style(authors, 'author', a)

        # database provider
        if "DB" in p:
            append_with_style(record, 'remote-database-provider', p.pop("DB"))

        # doi
        if "DO" in p:
            append_with_style(record, 'electronic-resource-num', p.pop("DO"))

        # edition
        if "ET" in p:
            append_with_style(record, 'edition', p.pop("ET"))

        # number
        if "IS" in p:
            append_with_style(record, 'number', p.pop("IS"))

        # titles
        has_titles = False
        titles = Element('titles')
        if "TI" in p:
            append_with_style(titles, 'title', p.pop("TI"))
            has_titles = True
        elif "T1" in p:
            append_with_style(titles, 'title', p.pop("T1"))
            has_titles = True

        if "T2" in p:
            append_with_style(titles, 'secondary-title', p.pop("T2"))
            has_titles = True

        if "J2" in p:
            append_with_style(titles, 'alt-title', p.pop("J2"))

        if has_titles:
            record.append(titles)

        # periodical
        if "J1" in p:
            periodical = SubElement(record, 'periodical')
            append_with_style(periodical, 'abbr-1', p.pop("J1"))
        elif "JA" in p:
            periodical = SubElement(record, 'periodical')
            append_with_style(periodical, 'abbr-1', p.pop("JA"))
        elif "JF" in p:
            periodical = SubElement(record, 'periodical')
            append_with_style(periodical, 'abbr-1', p.pop("JF"))

        # keywords
        if "KW" in p:
            keyword_list = p.pop("KW")
            try:
                keyword_list = json.loads(keyword_list)
            except ValueError:
                pass
            keywords = SubElement(record, 'keywords')
            for k in keyword_list:
                append_with_style(keywords, 'keyword', k)

        # language
        if "LA" in p:
            append_with_style(record, 'language', p.pop("LA"))

        # label
        if "LB" in p:
            append_with_style(record, 'label', p.pop("LB"))

        # notes
        if "N1" in p:
            note = p.pop("N1")
            try:
                note = json.loads(note)
                if isinstance(note, list):
                    note = note[0]
            except ValueError:
                pass
            append_with_style(record, 'notes', note)

        # dates
        has_date = False
        dates = Element('dates')
        if "PY" in p or "Y1" in p:
            if "PY" in p:
                year = p.pop("PY").split(" ")
            else:
                if "DA" in p:
                    year = p.pop("Y1") + " " + p.pop("DA")
                else:
                    year = p.pop("Y1")
                year = year.split(" ")
            date = None
            if len(year) > 1:
                date = " ".join(year[1:])
                year = year[0]
            else:
                year = year[0]

            if year.isdigit() and len(year) == 4:
                append_with_style(dates, 'year', year)
                has_date = True
            else:
                date = year

            if date:
                pub_dates = SubElement(dates, 'pub-dates')
                append_with_style(pub_dates, 'date', date)
                has_date = True

        if has_date:
            record.append(dates)

        # isbn
        if "SN" in p:
            append_with_style(record, 'isbn', p.pop("SN"))

        # pages
        if "SP" in p:
            append_with_style(record, 'pages', p.pop("SP"))

        # urls
        if "UR" in p:
            urls = SubElement(record, 'urls')
            related_urls = SubElement(urls, 'related-urls')
            append_with_style(related_urls, 'url', p.pop("UR"))

        # volume
        if "VL" in p:
            append_with_style(record, 'volume', p.pop("VL"))

        # unknown remaining fields
        for k in p.keys():
            element = SubElement(record, k)
            v = p.pop(k)
            if not isinstance(v, basestring):
                v = json.dumps(v)
            element.text = v

        records.append(record)

    root.append(records)

    return tostring(root)


class EndNoteRISMapper(Mapper):

    def __init__(self):
        super(EndNoteRISMapper, self).__init__(overwrite=True)
        self.element_names = "record"

    def attach_article_ids(self, xml_items):
        return [(i, {}) for i in xml_items]

    def extract_all(self, xml_soup_article, selector, fn=None):
        if fn is None:
            fn = self.get_text_if_not_null
        values = []
        for elem in xml_soup_article.select(selector):
            value = fn(elem)
            if value:
                values.append(value)
            elem.extract()
        return values

    def extract_one(self, xml_soup_article, selector):
        elem = xml_soup_article.select_one(selector)
        if elem:
            elem.extract()
            return self.get_text_if_not_null(elem)
        return None

    def get_author(self, xml):
        author = self.get_text_if_not_null(xml)
        if author:
            return author
        return None

    def get_keywords(self, xml):
        keywords = self.extract_all(xml, "keywords > keyword > style")
        keywords = filter(lambda k: k is not None, keywords)
        return keywords

    def transform_to_piano(self, xml_soup_article):

        article = OrderedDict()

        # ref type
        ref_type = xml_soup_article.select_one("ref-type")
        if isinstance(ref_type, Tag) and ref_type.has_attr("name"):
            article["TY"] = ref_type["name"]
        else:
            article["TY"] = "Generic"
        # abstract
        article["AB"] = self.extract_one(xml_soup_article, "abstract > style")
        # author address
        article["AD"] = self.extract_one(xml_soup_article, "auth-address > style")
        # accession number
        article["AN"] = self.extract_one(xml_soup_article, "accession-num > style")
        # authors
        article["AU"] = self.extract_all(xml_soup_article, 'contributors > authors > author > style', fn=self.get_author)
        # remote database
        article["DB"] = self.extract_one(xml_soup_article, "remote-database-provider > style")
        # doi
        article["DO"] = self.extract_one(xml_soup_article, "electronic-resource-num > style")
        # edition
        article["ET"] = self.extract_one(xml_soup_article, "edition > style")
        # number
        article["IS"] = self.extract_one(xml_soup_article, "number > style")
        # journal
        article["J1"] = self.extract_one(xml_soup_article, "periodical > abbr-1 > style")
        article["J2"] = self.extract_one(xml_soup_article, "titles > alt-title > style")
        # keywords
        article["KW"] = self.get_keywords(xml_soup_article)
        # language
        article["LA"] = self.extract_one(xml_soup_article, "language > style")
        # label
        article["LB"] = self.extract_one(xml_soup_article, "label > style")
        # notes
        notes = self.extract_one(xml_soup_article, "notes > style")
        if notes:
            article["N1"] = [notes]
        # dates
        year = self.extract_one(xml_soup_article, "dates > year > style")
        date = self.extract_one(xml_soup_article, "dates > pub-dates > date > style")
        p_date = None
        if year and date:
            p_date = year + " " + date
        elif year:
            p_date = year
        elif date:
            p_date = date
        article["PY"] = p_date
        # isbn
        article["SN"] = self.extract_one(xml_soup_article, "isbn > style")
        # pages
        article["SP"] = self.extract_one(xml_soup_article, "pages > style")
        # titles
        article["TI"] = self.extract_one(xml_soup_article, "titles > title > style")
        article["T2"] = self.extract_one(xml_soup_article, "titles > secondary-title > style")
        # url
        article["UR"] = self.extract_one(xml_soup_article, "urls style")
        # volume
        article["VL"] = self.extract_one(xml_soup_article, "volume > style")

        ris = ""
        for k in article.keys():
            if article[k] is None:
                continue
            if k in LIST_TYPE_TAGS:
                for v in article[k]:
                    ris += "%s  - %s\n" % (k, v)
            else:
                ris += "%s  - %s\n" % (k, article[k])
        if ris:
            ris += "ER  - "

        return ris


class EndNoteMapper(Mapper):

    def __init__(self):
        super(EndNoteMapper, self).__init__(overwrite=True)
        self.element_names = "record"

    def attach_article_ids(self, xml_items):
        articles = []
        for xml in xml_items:
            ext_id, pmid, ref_id = self.get_ids(xml)
            # title = self.get_text_if_not_null(xml.select_one("titles > title > style"))
            ids = {}
            if ref_id:
                ids["_id"] = ref_id
            elif pmid:
                ids["pmid"] = pmid
            # elif title:
            #     ids["attributes"] = {"title": title}
            #     isbn = self.get_text_if_not_null(xml.select_one("isbn > style"))
            #     pages = self.get_text_if_not_null(xml.select_one("pages > style"))
            #     volume = self.get_text_if_not_null(xml.select_one("volume > style"))
            #     if isbn:
            #         ids["attributes"]["isbn"] = isbn
            #     if pages:
            #         ids["attributes"]["pages"] = pages
            #     if volume:
            #         ids["attributes"]["volume"] = volume
            elif ext_id:
                ids["external_id"] = ext_id
            articles.append((xml, ids))

        return articles

    def get_label(self, xml):
        return self.get_text_if_not_null(xml.select_one("label > style"))

    def get_ext_id(self, xml):
        # return self.extract_one(xml, "accession-num > style")
        return self.get_text_if_not_null(xml.select_one("accession-num > style"))

    def get_piano_id(self, xml):
        return self.get_text_if_not_null(xml.select_one("piano-id"))

    def get_ids(self, xml):
        label = self.get_label(xml)
        ext_id = self.get_ext_id(xml)
        ref_id = self.get_piano_id(xml)
        pmid = None
        if label and ext_id and ("Pubmed" in label or "MEDLINE:" in ext_id):
            if "Pubmed" in label:
                pmid = ext_id
            elif "MEDLINE:" in ext_id:
                pmid = ext_id.replace("MEDLINE:", "")
            # ext_id = "PUBMED:" + pmid
        return ext_id, pmid, ref_id

    def extract_one(self, xml_soup_article, selector):
        elem = xml_soup_article.select_one(selector)
        if elem:
            elem.extract()
            return self.get_text_if_not_null(elem)
        return None

    def extract_all(self, xml_soup_article, selector, fn=None):
        if fn is None:
            fn = self.get_text_if_not_null
        values = []
        for elem in xml_soup_article.select(selector):
            value = fn(elem)
            if value:
                values.append(value)
            elem.extract()
        return values

    @staticmethod
    def extract_remaining_fields(xml_soup_article, article):
        record = xml_soup_article.find("record")
        if record:
            root = ET.fromstring(str(record))
            for child in root:
                if child.tag in ["piano_id"]:
                    continue
                if child.text and len(child.text.strip()) > 0:
                    article[child.tag] = child.text
                elif len(child) == 1:
                    if sum(1 for _ in child.iter("style")) == 1:
                        for i in child.iter("style"):
                            article[child.tag] = i.text
        return article

    def transform_to_piano(self, xml_soup_article):
        article = {}

        article["title"] = self.extract_one(xml_soup_article, "titles > title > style")
        article["secondary-title"] = self.extract_one(xml_soup_article, "titles > secondary-title > style")
        article["journal"] = self.extract_one(xml_soup_article, "titles > alt-title > style")
        article["published_date"] = self.get_published_date(xml_soup_article)
        article["authors"] = json.dumps(
            self.extract_all(xml_soup_article, 'contributors > authors > author > style', fn=self.get_author),
            ensure_ascii=False
        )
        article["abstract"] = self.extract_one(xml_soup_article, "abstract > style")
        article["keywords"] = self.get_keywords(xml_soup_article)
        article["author_address"] = self.extract_one(xml_soup_article, "auth-address > style")
        article["pages"] = self.extract_one(xml_soup_article, "pages > style")
        article["volume"] = self.extract_one(xml_soup_article, "volume > style")
        article["number"] = self.extract_one(xml_soup_article, "number > style")
        article["edition"] = self.extract_one(xml_soup_article, "edition > style")
        article["isbn"] = self.extract_one(xml_soup_article, "isbn > style")
        article["language"] = self.extract_one(xml_soup_article, "language > style")

        ext_id, pmid, ref_id = self.get_ids(xml_soup_article)
        if ext_id:
            article["external_id"] = ext_id
        if pmid:
            article["pmid"] = pmid
        if ref_id:
            article["_id"] = ref_id

        article["endnote_database"] = self.extract_one(xml_soup_article, "database")
        article["endnote_label"] = self.extract_one(xml_soup_article, "label > style")
        # article["endnote_xml"] = self.get_text_if_not_null(xml_soup_article)

        article = self.extract_remaining_fields(xml_soup_article, article)

        return article

    def get_published_date(self, xml):
        year = self.extract_one(xml, "dates > year > style")
        date = self.extract_one(xml, "dates > pub-dates > date > style")
        published_date = None
        if year and date:
            published_date = year + " " + date
        elif year:
            published_date = year
        elif date:
            published_date = date
        return published_date

    def get_author(self, xml):
        author = self.get_text_if_not_null(xml)
        if author:
            return author
        return None

    def get_keywords(self, xml):
        keywords = self.extract_all(xml, "keywords > keyword > style")
        keywords = filter(lambda k: k is not None, keywords)
        return ",".join(keywords)

import json
import xml.etree.ElementTree as Et
import xmltodict
from piano_utils.utils.flatten_json import flatten, unflatten_list


class ConversionError(Exception):
    pass


def xml_to_json(xml_string, element_name=None):
    """
    Converts an XML string to a flattened JSON structure
    :param xml_string: XML string to flatten into JSON
    :param element_name: (optional) XML element name from which the child elements are converted
    :return: JSON
    """
    json_list = []

    try:
        if element_name:
            root = Et.fromstring(xml_string)
            for element in root.iter(element_name):
                xml_dict = xmltodict.parse(Et.tostring(element))
                json_list.append(flatten(xml_dict.get(element_name, {})))
        else:
            json_list.append(flatten(xmltodict.parse(xml_string)))

        return json.dumps(json_list)
    except Exception as e:
        raise ConversionError(e.message)


def json_to_xml(json_string, root_names=None, element_name=None):
    """
    Converts a JSON string to an XML string
    :param json_string: JSON string to convert into XML
    :param root_names: (optional) root element name in the XML;
     can be string or list of strings (for multiple parent elements)
    :param element_name: (optional) element name of children
    :return: XML
    """
    try:
        json_value = json.loads(json_string)
    except ValueError as e:
        raise ConversionError("Invalid JSON string: %s" % e)

    try:
        if isinstance(json_value, list):
            if len(json_value) > 1:
                json_dicts = []
                for json_string in json_value:
                    json_dicts.append(unflatten_list(json_string))

                json_value = json_dicts
                if element_name:
                    json_value = {element_name: json_dicts}
                if root_names:
                    if isinstance(root_names, list):
                        for n in reversed(root_names):
                            json_value = {n: json_value}
                    else:
                        json_value = {root_names: json_value}
                return xmltodict.unparse(json_value)
            else:
                json_value = json_value[0]

        if isinstance(json_value, dict):
            json_value = unflatten_list(json_value)
            if element_name:
                json_value = {element_name: json_value}
            if root_names:
                if isinstance(root_names, list):
                    for n in reversed(root_names):
                        json_value = {n: json_value}
                else:
                    json_value = {root_names: json_value}
            return xmltodict.unparse(json_value)
    except Exception as e:
        raise ConversionError(e.message)

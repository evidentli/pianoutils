from io import BytesIO
from collections import OrderedDict
import csv


def csv_to_piano(csv_content, id_name=None):
    """
    Converts csv string to a list of piano dictionaries
    :param csv_content: CSV string to convert
    :param id_name: primary key name (default: _id)
    :return: list
    """

    if id_name is None:
        id_name = "_id"

    f = BytesIO(csv_content)
    csv_reader = csv.reader(f)

    headers = None
    references = OrderedDict()
    for row in csv_reader:
        if not headers:
            if row:
                if not row[1]:
                    break
                headers = row
            continue

        if row:
            _id = row[0]
            reference = {id_name: _id}
            for idx, header in enumerate(headers[1:], start=1):
                if header:
                    reference[header] = row[idx]
            references[_id] = reference

    return references.values()

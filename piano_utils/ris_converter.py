import json
from RISparser import read
from RISparser.config import LIST_TYPE_TAGS


RIS_KEY_MAPPING = {
    "TY": "Type of reference (must be the first tag)",
    "A1": "First Author",
    "A2": "Secondary Author (each author on its own line preceded by the tag)",
    "A3": "Tertiary Author (each author on its own line preceded by the tag)",
    "A4": "Subsidiary Author (each author on its own line preceded by the tag)",
    "AB": "Abstract",
    "AD": "Author Address",
    "AN": "Accession Number",
    "AU": "Author (each author on its own line preceded by the tag)",
    "AV": "Location in Archives",
    "BT": "This field maps to T2 for all reference types except for Whole Book and Unpublished Work references. It can contain alphanumeric characters. There is no practical limit to the length of this field.",
    "C1": "Custom 1",
    "C2": "Custom 2",
    "C3": "Custom 3",
    "C4": "Custom 4",
    "C5": "Custom 5",
    "C6": "Custom 6",
    "C7": "Custom 7",
    "C8": "Custom 8",
    "CA": "Caption",
    "CN": "Call Number",
    "CP": "This field can contain alphanumeric characters. There is no practical limit to the length of this field.",
    "CT": "Title of unpublished reference",
    "CY": "Place Published",
    "DA": "Date",
    "DB": "Name of Database",
    "DO": "DOI",
    "DP": "Database Provider",
    "ED": "Editor",
    "EP": "End Page",
    "ET": "Edition",
    "ID": "Reference ID",
    "IS": "Issue number",
    "J1": "Periodical name: user abbreviation 1. This is an alphanumeric field of up to 255 characters.",
    "J2": "Alternate Title (this field is used for the abbreviated title of a book or journal name, the latter mapped to T2)",
    "JA": "Periodical name: standard abbreviation. This is the periodical in which the article was (or is to be, in the case of in-press references) published. This is an alphanumeric field of up to 255 characters.",
    "JF": "Journal/Periodical name: full format. This is an alphanumeric field of up to 255 characters.",
    "JO": "Journal/Periodical name: full format. This is an alphanumeric field of up to 255 characters.",
    "KW": "Keywords (keywords should be entered each on its own line preceded by the tag)",
    "L1": "Link to PDF. There is no practical limit to the length of this field. URL addresses can be entered individually, one per tag or multiple addresses can be entered on one line using a semi-colon as a separator.",
    "L2": "Link to Full-text. There is no practical limit to the length of this field. URL addresses can be entered individually, one per tag or multiple addresses can be entered on one line using a semi-colon as a separator.",
    "L3": "Related Records. There is no practical limit to the length of this field.",
    "L4": "Image(s). There is no practical limit to the length of this field.",
    "LA": "Language",
    "LB": "Label",
    "LK": "Website Link",
    "M1": "Number",
    "M2": "Miscellaneous 2. This is an alphanumeric field and there is no practical limit to the length of this field.",
    "M3": "Type of Work",
    "N1": "Notes",
    "N2": "Abstract. This is a free text field and can contain alphanumeric characters. There is no practical length limit to this field.",
    "NV": "Number of Volumes",
    "OP": "Original Publication",
    "PB": "Publisher",
    "PI": "Piano ID",
    "PP": "Publishing Place",
    "PY": "Publication year (YYYY/MM/DD)",
    "RI": "Reviewed Item",
    "RN": "Research Notes",
    "RP": "Reprint Edition",
    "SE": "Section",
    "SN": "ISBN/ISSN",
    "SP": "Start Page",
    "ST": "Short Title",
    "T1": "Primary Title",
    "T2": "Secondary Title (journal title, if applicable)",
    "T3": "Tertiary Title",
    "TA": "Translated Author",
    "TI": "Title",
    "TT": "Translated Title",
    "U1": "User definable 1. This is an alphanumeric field and there is no practical limit to the length of this field.",
    "U2": "User definable 2. This is an alphanumeric field and there is no practical limit to the length of this field.",
    "U3": "User definable 3. This is an alphanumeric field and there is no practical limit to the length of this field.",
    "U4": "User definable 4. This is an alphanumeric field and there is no practical limit to the length of this field.",
    "U5": "User definable 5. This is an alphanumeric field and there is no practical limit to the length of this field.",
    "UK": "unknown tag",
    "UR": "URL",
    "VL": "Volume number",
    "VO": "Published Standard number",
    "Y1": "Primary Date",
    "Y2": "Access Date",
    "ER": "End of Reference (must be empty and the last tag)",
}
PIANO_KEY_MAPPING = {k: k for k in RIS_KEY_MAPPING.keys()}


def ris_to_piano(ris_string):
    """
    Converts a RIS string to a list of Piano dictionaries
    :param ris_string: RIS string to convert
    :return: list
    """
    if not isinstance(ris_string, unicode):
        ris_string = ris_string.decode("utf-8")
    if ris_string and len(ris_string) > 1:
        while ris_string[0:2] != "TY":
            ris_string = ris_string[1:]
    ris_string = ris_string.replace("ER  -", "ER  - ")
    piano = list(read(ris_string.splitlines(), PIANO_KEY_MAPPING))
    for ref in piano:
        unknown_tags = {}
        if "UK" in ref:
            unknown_tags = ref.pop("UK")
            ref.update(unknown_tags)
        if "PI" in ref:
            ref["_id"] = ref.pop("PI")
        for k, v in ref.iteritems():
            if not isinstance(v, basestring):
                if isinstance(v, list):
                    if k in LIST_TYPE_TAGS:
                        ref[k] = json.dumps(v)
                    elif k in unknown_tags.keys() and len(v) > 1:
                        ref[k] = json.dumps(v)
                    else:
                        ref[k] = " ".join(v)
                else:
                    ref[k] = str(v)
    return piano


def piano_to_ris(piano):
    """
    Converts a list of Piano dictionaries to an RIS string
    :param piano: list/dict of piano articles/references to convert
    :return:
    """
    def append_tag(tag, d):
        if tag in d:
            val = d.pop(tag)
            if tag == "_id":
                tag = "PI"
            is_list = False
            try:
                if isinstance(json.loads(val), list):
                    is_list = True
            except Exception:
                pass
            if tag in LIST_TYPE_TAGS or is_list:
                try:
                    val = json.loads(val)
                    return "".join(["%s  - %s\n" % (tag, i) for i in val])
                except ValueError:
                    pass
            return "%s  - %s\n" % (tag, val)
        return ""

    if isinstance(piano, dict):
        piano = [piano]

    if not isinstance(piano, list):
        return []

    ris_string = ""
    for ref in piano:
        if "TY" not in ref:
            continue
        ris_string += append_tag("TY", ref)
        for t in sorted(ref.keys()):
            if len(t) != 2 and t != "_id":
                continue
            ris_string += append_tag(t, ref)
        ris_string += "ER  - \n"

    return ris_string

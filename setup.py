from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='piano_utils',
    packages=find_packages(exclude=["tests"]),
    version='0.8.1',
    description='Piano Utilities',
    author='Michael Van Treeck',
    author_email='michael@evidentli.com',
    url='https://bitbucket.org/evidentli/pianoutils',
    license='MIT',
    keywords=['piano', 'util', 'xml', 'json', 'pubmed', 'endnote', 'ris', 'csv'],
    classifiers=[],
    install_requires=requirements,
)
